﻿using AutoMapper;
using CapstoneMVC.Dtos;
using CapstoneMVC.Models;

namespace CapstoneMVC.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Individual, IndividualDto>();
            Mapper.CreateMap<UserType, UserTypeDto>();
            Mapper.CreateMap<TortCase, TortCaseDto>();
            Mapper.CreateMap<CompanionshipType, CompanionshipTypeDto>();




            Mapper.CreateMap<IndividualDto, Individual>();
            Mapper.CreateMap<UserTypeDto, UserType>();
            Mapper.CreateMap<TortCaseDto, TortCase>();
            Mapper.CreateMap<CompanionshipTypeDto, CompanionshipType>();
        }
    }
}