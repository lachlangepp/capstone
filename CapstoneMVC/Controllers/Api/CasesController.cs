﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using CapstoneMVC.Dtos;
using CapstoneMVC.Models;
using Microsoft.AspNet.Identity;

namespace CapstoneMVC.Controllers.Api
{
    public class CasesController : ApiController
    {
        private ApplicationDbContext _context;

        public CasesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET /api/tortcases
        public IHttpActionResult GetTortCases()
        {
            string userId = User.Identity.GetUserId();
            //get user individualID to get case list...
            //var current_individualID = _context.Individual.SqlQuery("SELECT * FROM dbo.Individuals INNER JOIN dbo.AspNetUsers ON dbo.Individuals.IndividualID = dbo.AspNetUsers.IndividualID WHERE dbo.AspNetUsers.Id = " + userId).FirstOrDefault();
            //string command = "SELECT IndividualID FROM dbo.AspNetUsers WHERE dbo.AspNetUsers.Id = " + userId;
            //int _current_individualID = 0;
            //_current_individualID = _context.Individual.
            //var current_individualID = _context.Individual.SqlQuery("SELECT IndividualID FROM dbo.AspNetUsers WHERE dbo.AspNetUsers.Id = " + "'"+userId+"'").Single();
            //var current_individualID = 2;

            
            //var command = new SqlCommand("getuserid",_context);
            //command.CommandType = CommandType.StoredProcedure;
            //command.Parameters.AddWithValue("@param1", userId);
            
            //_context.Open();
            //command.ExcuteNonQuery();
            //connection.Close();


            var tortCaseDtos = _context.TortCase
                .SqlQuery("SELECT * FROM dbo.TortCases INNER JOIN dbo.Individuals ON dbo.TortCases.TortCaseID = dbo.Individuals.TortCaseID WHERE dbo.Individuals.IndividualID = 2")
                .Select(Mapper.Map<TortCase, TortCaseDto>);

            // checks to see if user is admin
            // will then return only the suitable cases
            if (userId != "01c14609-32ff-43bd-bd40-87f7d34c6962")
            {
                return Ok(tortCaseDtos);
            }
            // if it is the admin return all the cases in database
                tortCaseDtos = _context.TortCase
                    .Select(Mapper.Map<TortCase, TortCaseDto>);

            return Ok(tortCaseDtos);

        }

        // GET /api/tortcases/1
        public IHttpActionResult GetTortCase(int id)
        {
            var tortCase = _context.TortCase.SingleOrDefault(c => c.TortCaseID == id);

            if (tortCase == null)
                return NotFound();

            return Ok(Mapper.Map<TortCase, TortCaseDto>(tortCase));
        }

        // POST /api/tortcases
        [HttpPost]
        public IHttpActionResult CreateTortCase(TortCaseDto tortCaseDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var tortCase = Mapper.Map<TortCaseDto, TortCase>(tortCaseDto);
            _context.TortCase.Add(tortCase);
            _context.SaveChanges();

            tortCaseDto.TortCaseID = tortCase.TortCaseID;

            return Created(new Uri(Request.RequestUri + "/" + tortCase.TortCaseID), tortCaseDto);
        }

        // PUT /api/tortcases/1
        [HttpPut]
        public IHttpActionResult UpdateTortCase(int id, TortCaseDto tortCaseDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var tortCaseInDb = _context.TortCase.SingleOrDefault(c => c.TortCaseID == id);

            if (tortCaseInDb == null)
                return NotFound();

            Mapper.Map(tortCaseDto, tortCaseInDb);

            _context.SaveChanges();

            return Ok();
        }

        // DELETE /api/tortcases/1
        [HttpDelete]
        public IHttpActionResult DeleteTortCase(int id)
        {
            var tortCaseInDb = _context.TortCase.SingleOrDefault(c => c.TortCaseID == id);

            if (tortCaseInDb == null)
                return NotFound();

            _context.TortCase.Remove(tortCaseInDb);
            _context.SaveChanges();

            return Ok();
        }
    }
}
