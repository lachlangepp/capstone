﻿using System;
using System.Linq;
using AutoMapper;
using CapstoneMVC.Dtos;
using CapstoneMVC.Models;
using System.Data.Entity;
using System.Web.Http;

namespace CapstoneMVC.Controllers.Api
{
    public class ClientsController : ApiController
    {
        private ApplicationDbContext _context;

        public ClientsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET /api/clients
        public IHttpActionResult GetTortIndividuals()
        {

            var individualDtos = _context.Individual
                //.Include(c => c.UserType)
                .SqlQuery("SELECT * FROM dbo.Individuals WHERE NOT UserTypeID=1 AND NOT UserTypeID=3")
                .ToList()
                .Select(Mapper.Map<Individual, IndividualDto>);

            return Ok(individualDtos);
        }

        // GET /api/clients/1
        public IHttpActionResult GetTortIndividuals(int id)
        {
            var individual = _context.Individual.SingleOrDefault(c => c.IndividualID == id);

            if (individual == null)
                return NotFound();

            return Ok(Mapper.Map<Individual, IndividualDto>(individual));
        }

        // POST /api/clients
        [HttpPost]
        public IHttpActionResult CreateTortIndividuals(IndividualDto individualDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var individual = Mapper.Map<IndividualDto, Individual>(individualDto);
            _context.Individual.Add(individual);
            _context.SaveChanges();

            individualDto.IndividualID = individual.IndividualID;

            return Created(new Uri(Request.RequestUri + "/" + individual.IndividualID), individualDto);
        }

        //// PUT /api/clients/1
        //[HttpPut]
        //public IHttpActionResult UpdateTortCase(int id, TortCaseDto tortCaseDto)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest();

        //    var tortCaseInDb = _context.TortCase.SingleOrDefault(c => c.TortCaseId == id);

        //    if (tortCaseInDb == null)
        //        return NotFound();

        //    Mapper.Map(tortCaseDto, tortCaseInDb);

        //    _context.SaveChanges();

        //    return Ok();
        //}

        //// DELETE /api/clients/1
        //[HttpDelete]
        //public IHttpActionResult DeleteTortCase(int id)
        //{
        //    var tortCaseInDb = _context.TortCase.SingleOrDefault(c => c.TortCaseId == id);

        //    if (tortCaseInDb == null)
        //        return NotFound();

        //    _context.TortCase.Remove(tortCaseInDb);
        //    _context.SaveChanges();

        //    return Ok();
        //}
    }
}
