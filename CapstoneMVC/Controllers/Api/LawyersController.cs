﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using CapstoneMVC.Dtos;
using CapstoneMVC.Models;

namespace CapstoneMVC.Controllers.Api
{
    public class LawyersController : ApiController
    {
        private ApplicationDbContext _context;

        public LawyersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET /api/lawyers
        public IHttpActionResult GetTortLawyers()
        {

            var individualDtos = _context.Individual
                //.Include(c => c.UserType)
                .SqlQuery("SELECT * FROM dbo.Individuals WHERE UserTypeID = 3")
                .ToList()
                .Select(Mapper.Map<Individual, IndividualDto>);

            return Ok(individualDtos);
        }

        // GET /api/lawyers/1
        public IHttpActionResult GetTortLawyers(int id)
        {
            var individual = _context.Individual.SingleOrDefault(c => c.IndividualID == id);

            if (individual == null)
                return NotFound();

            return Ok(Mapper.Map<Individual, IndividualDto>(individual));
        }

        // POST /api/lawyers
        [HttpPost]
        public IHttpActionResult CreateTortLawyers(IndividualDto individualDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var individual = Mapper.Map<IndividualDto, Individual>(individualDto);
            _context.Individual.Add(individual);
            _context.SaveChanges();

            individualDto.IndividualID = individual.IndividualID;

            return Created(new Uri(Request.RequestUri + "/" + individual.IndividualID), individualDto);
        }

        // PUT /api/lawyers/1
        [HttpPut]
        public IHttpActionResult UpdateTortLawyer(int id, IndividualDto individualDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var individualInDb = _context.Individual.SingleOrDefault(c => c.IndividualID == id);

            if (individualInDb == null)
                return NotFound();

            Mapper.Map(individualDto, individualInDb);

            _context.SaveChanges();

            return Ok();
        }

        // DELETE /api/lawyers/1
        [HttpDelete]
        public IHttpActionResult DeleteTortLawyer(int id)
        {
            var individualInDb = _context.Individual.SingleOrDefault(c => c.IndividualID == id);

            if (individualInDb == null)
                return NotFound();

            _context.Individual.Remove(individualInDb);
            _context.SaveChanges();

            return Ok();
        }
    }
}
