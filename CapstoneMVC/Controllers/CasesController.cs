﻿using System;
using System.Linq;
using System.Web.Mvc;
using CapstoneMVC.Models;
using CapstoneMVC.ViewModels;

namespace CapstoneMVC.Controllers
{
    public class CasesController : Controller
    {
        private ApplicationDbContext _context;
        public CasesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Cases
        public ActionResult Index()
        {

            //var tortCase = _context.TortCase.ToList();

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public ActionResult NewCase()
        {

            var viewModel = new CaseFormViewModel();

            return View("NewCase", viewModel);
        }

        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public ActionResult Edit(int id)
        {
            var tortCase = _context.TortCase.SingleOrDefault(c => c.TortCaseID == id);

            if (tortCase == null)
                return HttpNotFound();

            var viewModel = new CaseFormViewModel(tortCase);

            return View("NewCase", viewModel);
        }

        public ActionResult Details(int id)
        {
            var tortCase = _context.TortCase.SingleOrDefault(c => c.TortCaseID == id);

            if (tortCase == null)
                return HttpNotFound();

            return View(tortCase);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public ActionResult Save(TortCase tortCase)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new CaseFormViewModel(tortCase);

                return View("NewCase", viewModel);
            }

            if (tortCase.TortCaseID == 0)
            {
                tortCase.DateAdded = DateTime.Now;
                _context.TortCase.Add(tortCase);
            }
            else
            {
                var caseInDb = _context.TortCase.Single(m => m.TortCaseID == tortCase.TortCaseID);
                caseInDb.CaseName = tortCase.CaseName;
                caseInDb.CaseDescription = tortCase.CaseDescription;
                caseInDb.CaseIdentifier = tortCase.CaseIdentifier;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Cases");
        }
    }
}