﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapstoneMVC.Models;
using CapstoneMVC.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace CapstoneMVC.Controllers
{
    public class ClientsController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationDbContext _context;

        public ClientsController()
        {
            _context = new ApplicationDbContext();
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Cases
        public ActionResult Index()
        {

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public ActionResult NewClient()
        {

            var viewModel = new IndividualFormViewModel();

            return View("ClientForm", viewModel);
        }

        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public ActionResult Edit(int id)
        {
            var tortClients = _context.Individual.SingleOrDefault(c => c.IndividualID == id);

            if (tortClients == null)
                return HttpNotFound();

            var viewModel = new IndividualFormViewModel(tortClients);

            return View("ClientForm", viewModel);
        }

        public ActionResult Details(int id)
        {
            var tortClients = _context.Individual.SingleOrDefault(c => c.IndividualID == id);

            if (tortClients == null)
                return HttpNotFound();

            return View(tortClients);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public async System.Threading.Tasks.Task<ActionResult> Save(Individual individual)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new IndividualFormViewModel(individual);

                return View("ClientForm", viewModel);
            }

            if (individual.IndividualID == 0)
            {
                individual.DateAdded = DateTime.Now;
                _context.Individual.Add(individual);
            }
            else
            {
                var individualInDb = _context.Individual.Single(m => m.IndividualID == individual.IndividualID);

                individualInDb.Email = individual.Email;
                individualInDb.UserFirst = individual.UserFirst;
                individualInDb.UserLast = individual.UserLast;
                individualInDb.UserMiddle = individual.UserMiddle;
                individualInDb.UserMaiden = individual.UserMaiden;
                individualInDb.Gender = individual.Gender;
                individualInDb.Occupation = individual.Occupation;
                individualInDb.DateOfBirth = individual.DateOfBirth;
                individualInDb.PlaceOfBirth = individual.PlaceOfBirth;
                individualInDb.Dependent = individual.Dependent;
                individualInDb.EmployerName = individual.EmployerName;
                individualInDb.EmployerNumber = individual.EmployerNumber;
                individualInDb.PhoneNumber = individual.PhoneNumber;
                individualInDb.DateAdded = individual.DateAdded;
                individualInDb.GuardianshipDetails = individual.GuardianshipDetails;
                individualInDb.IndividualRelationID = individual.IndividualRelationID;
                individualInDb.TortCaseID = individual.TortCaseID;
                individualInDb.UserTypeID = individual.UserTypeID;
                individualInDb.CompanionshipTypeID = individual.CompanionshipTypeID;
                //newIndividual.IndividualRelatedTo = newIndividual;
                individualInDb.EmailConfirmed = individual.EmailConfirmed;
                //_context.Individual.Add(individualInDb);

                // UPDATE EMAIL CONFIRMED>>>> Manually by lawyer
                var aspnetuserInDb = UserManager.FindByEmail(individualInDb.Email); // this finds a user by the email in the model view. CORRECT
                aspnetuserInDb.EmailConfirmed = individualInDb.EmailConfirmed;
                UserManager.Update(aspnetuserInDb);
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Clients");
        }
    }
}