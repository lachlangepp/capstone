﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapstoneMVC.Models;
using CapstoneMVC.ViewModels;

namespace CapstoneMVC.Controllers
{
    public class LawyersController : Controller
    {

        private ApplicationDbContext _context;
        public LawyersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Lawyers
        public ActionResult Index()
        {

            //var tortClients = _context.tortClients.ToList();

            return View();
        }

        public ActionResult DBquery()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public ActionResult NewLawyer()
        {

            var viewModel = new IndividualFormViewModel();

            return View("LawyerForm", viewModel);
        }

        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public ActionResult Edit(int id)
        {
            var tortLawyers = _context.Individual.SingleOrDefault(c => c.IndividualID == id);

            if (tortLawyers == null)
                return HttpNotFound();

            var viewModel = new IndividualFormViewModel(tortLawyers);

            return View("LawyerForm", viewModel);
        }

        public ActionResult Details(int id)
        {
            var tortLawyers = _context.Individual.SingleOrDefault(c => c.IndividualID == id);

            if (tortLawyers == null)
                return HttpNotFound();

            return View(tortLawyers);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.TortAdminandLawyer)]
        public ActionResult Save(Individual tortLawyers)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new IndividualFormViewModel(tortLawyers);

                return View("LawyerForm", viewModel);
            }

            if (tortLawyers.IndividualID == 0)
            {
                tortLawyers.DateAdded = DateTime.Now;
                _context.Individual.Add(tortLawyers);
            }
            else
            {
                var individualInDb = _context.Individual.Single(m => m.IndividualID == tortLawyers.IndividualID);
                //individualInDb.
                //var caseInDb = _context.TortCase.Single(m => m.TortCaseId == tortCase.TortCaseId);
                individualInDb.UserFirst = individualInDb.UserFirst;
                //caseInDb.CaseDescription = tortCase.CaseDescription;
                //caseInDb.CaseIdentifier = tortCase.CaseIdentifier;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Lawyers");
        }
    }
}