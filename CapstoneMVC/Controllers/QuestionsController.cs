﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CapstoneMVC.Models;
using CapstoneMVC.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace CapstoneMVC.Controllers
{
    public class QuestionsController : Controller
    {

        private ApplicationUserManager _userManager;
        private ApplicationDbContext _context;
        // GET: Questions
        //[Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            //var companionshipTypes = _context.CompanionshipType.ToList();
            //var userTypes = _context.UserType.ToList();
            //var addressTypes = _context.AddressType.ToList();
            //var documentTypes = _context.DocumentType.ToList();

            //var viewModel = new QuestionsViewModel
            //{
            //    CompanionshipTypes = companionshipTypes,
            //    UserTypes = userTypes,
            //    AddressTypes = addressTypes,
            //    DocumentTypes = documentTypes
            //};

            return View();
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpPost]
        public async Task<ActionResult> New(QuestionsViewModel model)
        {
            //ApplicationDbContext _context = new ApplicationDbContext();

            var indID = await UserManager.FindByEmailAsync(User.Identity.Name);
            try
            {

                _context = new ApplicationDbContext();
                //List<CompanionshipType> list = _context.CompanionshipType.ToList();
                //ViewBag.DepartmentList = new SelectList(list, "DepartmentId", "DepartmentName");

                // put things in lists that need to be in lists??

                //pull in this modelview from db so when questionnaire loads,
                //previous entries are laoded... (continue where they left off)

                // PUT ALL MODEL INPUTS HERE
                // assign all entries to their model.counterparts
                // add to _context
                // _context.savechanges

                // gets the current individualID from db.
                Individual individual = await _context.Individual.SingleOrDefaultAsync(m => m.IndividualID == indID.IndividualID);
                var latestID = individual.IndividualID;
                int relatedtortCase = individual.TortCaseID;

                individual.UserFirst = model.UserFirst;
                individual.UserLast = model.UserLast;
                individual.UserMiddle = model.UserMiddle;
                individual.UserMaiden = model.UserMaiden;
                individual.Gender = model.Gender;
                individual.Occupation = "INDIVIDUAL";
                if (model.DateOfBirth.HasValue)
                    individual.DateOfBirth = model.DateOfBirth.Value;
                individual.PlaceOfBirth = model.PlaceOfBirth;
                individual.Dependent = individual.Dependent;
                individual.EmployerName = model.vicEmployerName;
                individual.EmployerNumber = individual.EmployerNumber;
                individual.PhoneNumber = individual.PhoneNumber;
                individual.Email = indID.Email;
                individual.DateAdded = System.DateTime.Now;
                individual.GuardianshipDetails = "INDIVIDUAL";

                individual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                individual.TortCaseID = relatedtortCase;
                individual.UserTypeID = individual.UserTypeID;
                individual.CompanionshipTypeID = model.CompanionshipTypeID;
                individual.IndividualRelatedTo = null;
                individual.EmailConfirmed = individual.EmailConfirmed;

                // updating the first user instead of adding a new one...
                var aspnetuserInDb = UserManager.FindByEmail(individual.Email); // this finds a user by the email in the model view. CORRECT
                UserManager.Update(aspnetuserInDb);

                //_context.Individual.Add(individual);
                _context.SaveChanges();

                Address addressResidential = new Address();
                addressResidential.IndividualID = latestID;
                addressResidential.AddressTypeID = 2;
                addressResidential.StreetNumber = model.StreetNumber;
                addressResidential.StreetName = model.StreetName;
                addressResidential.Suburb = model.Suburb;
                addressResidential.PostCode = model.PostCode;
                addressResidential.State = model.State;
                addressResidential.Country = model.Country;

                _context.Address.Add(addressResidential);
                _context.SaveChanges();

                //// spouse individual
                Individual spouseIndividual = new Individual();
                spouseIndividual.UserTypeID = 5;
                spouseIndividual.UserFirst = model.spouseUserFirst;
                spouseIndividual.UserLast = model.spouseUserLast;
                spouseIndividual.UserMiddle = model.spouseUserMiddle;
                spouseIndividual.UserMaiden = model.spouseUserMaiden;
                if (true)
                    spouseIndividual.DateOfBirth = System.DateTime.Now;
                spouseIndividual.PlaceOfBirth = "SPOUSE";
                spouseIndividual.Gender = "SPOUSE";
                spouseIndividual.DateAdded = System.DateTime.Now;
                spouseIndividual.GuardianshipDetails = "SPOUSE";
                spouseIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                spouseIndividual.TortCaseID = relatedtortCase;
                spouseIndividual.CompanionshipTypeID = model.CompanionshipTypeID;
                spouseIndividual.PhoneNumber = model.spousePhoneNumber;
                spouseIndividual.Email = model.spouseEmail;
                spouseIndividual.Occupation = model.spouseOccupation;

                _context.Individual.Add(spouseIndividual);
                _context.SaveChanges();
                int VLSID = spouseIndividual.IndividualID;

            //    PostMortemService spousedead = new PostMortemService();
            //    spousedead.IndividualID = VLSID;
            //    spousedead.cos
            ////date of death of spouse???
            //// spouse dead bool??

                //_context.PostMortemService.Add(spousedead);
                //_context.SaveChanges();

                Address VLSaddress = new Address();
                VLSaddress.AddressTypeID = 2;
                VLSaddress.IndividualID = VLSID;
                VLSaddress.StreetNumber = model.VLSStreetNumber;
                VLSaddress.StreetName = model.VLSStreetName;
                VLSaddress.Suburb = model.VLSSuburb;
                VLSaddress.State = model.VLSState;
                VLSaddress.Country = model.VLSCountry;

                _context.Address.Add(VLSaddress);
                _context.SaveChanges();

                DefactoCompanion defactoIndividual = new DefactoCompanion();
                defactoIndividual.IndividualID = VLSID;
                defactoIndividual.RelationshipContinued = model.RelationshipContinued;
                defactoIndividual.DateOfCommencementOfRelationship = model.DateOfCommencementOfRelationship;
                defactoIndividual.NumberOfChildren = model.NumberOfChildren;
                //wedding cert
                //spouse birth cert
                //companion birth cert
                //defactoIndividual.DateOfMarriage = model.DateOfMarriage;
                defactoIndividual.FacsimileNumber = model.spouseFacsimile;
                // where to put companion facsimile
                //defactoIndividual.FacsimileNumber = model.compFacsimile;

                _context.DefactoCompanion.Add(defactoIndividual);
                _context.SaveChanges();

                ////companion individual
                Individual compIndividual = new Individual();
                compIndividual.UserTypeID = 8;
                compIndividual.UserFirst = model.compUserFirst;
                compIndividual.UserLast = model.compUserLast;
                compIndividual.UserMiddle = model.compUserMiddle;
                compIndividual.UserMaiden = model.compUserMaiden;
                if (model.DateOfBirth.HasValue)
                    compIndividual.DateOfBirth = System.DateTime.Now;
                compIndividual.PlaceOfBirth = "COMPANION";
                compIndividual.Gender = "COMPANION";
                compIndividual.DateAdded = System.DateTime.Now;
                compIndividual.GuardianshipDetails = "COMPANION";
                compIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                compIndividual.TortCaseID = relatedtortCase;
                compIndividual.CompanionshipTypeID = 2;
                compIndividual.PhoneNumber = model.compPhoneNumber;
                compIndividual.Email = model.compEmail;
                compIndividual.Occupation = model.compOccupation;
                compIndividual.EmployerName = model.compEmployerName;

                _context.Individual.Add(compIndividual);
                _context.SaveChanges();
                int compID = compIndividual.IndividualID;

                ////companion address's
                Address compaddress = new Address();
                compaddress.AddressTypeID = 2;
                compaddress.IndividualID = compID;
                compaddress.StreetNumber = model.compStreetNumber;
                compaddress.StreetName = model.compStreetName;
                compaddress.Suburb = model.compSuburb;
                compaddress.State = model.compState;
                compaddress.Country = model.compCountry;

                _context.Address.Add(compaddress);
                _context.SaveChanges();

                Address postalcompaddress = new Address();
                postalcompaddress.AddressTypeID = 1;
                postalcompaddress.IndividualID = compID;
                postalcompaddress.StreetNumber = model.postalcompStreetNumber;
                postalcompaddress.StreetName = model.postalcompStreetName;
                postalcompaddress.Suburb = model.postalcompSuburb;
                postalcompaddress.State = model.postalcompState;
                postalcompaddress.Country = model.postalcompCountry;

                _context.Address.Add(postalcompaddress);
                _context.SaveChanges();

                ////client individual
                Individual clientIndividual = new Individual();
                clientIndividual.UserTypeID = model.clientvicUserTypeID;
                clientIndividual.UserFirst = model.clientUserFirst;
                clientIndividual.UserLast = model.clientUserLast;
                clientIndividual.UserMiddle = model.clientUserMiddle;
                clientIndividual.UserMaiden = model.clientUserMaiden;
                if (model.DateOfBirth.HasValue)
                    clientIndividual.DateOfBirth = model.clientDateOfBirth.Value;
                clientIndividual.PlaceOfBirth = model.clientPlaceOfBirth;
                clientIndividual.Gender = model.clientGender;
                clientIndividual.DateAdded = System.DateTime.Now;
                clientIndividual.GuardianshipDetails = model.GuardianshipDetails;
                clientIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                clientIndividual.TortCaseID = relatedtortCase;
                clientIndividual.CompanionshipTypeID = 1;
                clientIndividual.PhoneNumber = model.clientCellPhoneNumber;
                clientIndividual.Email = "CLIENT";
                clientIndividual.Occupation = "CLIENT";
                clientIndividual.EmployerName = "CLIENT";

                _context.Individual.Add(clientIndividual);
                _context.SaveChanges();
                int clientID = clientIndividual.IndividualID;

                //client address's
                Address resclientaddress = new Address();
                resclientaddress.AddressTypeID = 2;
                resclientaddress.IndividualID = clientID;
                resclientaddress.StreetNumber = model.resclientStreetNumber;
                resclientaddress.StreetName = model.resclientStreetName;
                resclientaddress.Suburb = model.resclientSuburb;
                resclientaddress.State = model.resclientState;
                resclientaddress.Country = model.resclientCountry;

                _context.Address.Add(resclientaddress);
                _context.SaveChanges();

                //client postal address's
                Address postalclientaddress = new Address();
                postalclientaddress.AddressTypeID = 2;
                postalclientaddress.IndividualID = clientID;
                postalclientaddress.StreetNumber = model.postalclientStreetNumber;
                postalclientaddress.StreetName = model.postalclientStreetName;
                postalclientaddress.Suburb = model.postalclientSuburb;
                postalclientaddress.State = model.postalclientState;
                postalclientaddress.Country = model.postalclientCountry;

                _context.Address.Add(postalclientaddress);
                _context.SaveChanges();

                //client event address's
                Address eventclientaddress = new Address();
                eventclientaddress.AddressTypeID = 1; // add new address type for address at time of accident
                eventclientaddress.IndividualID = clientID;
                eventclientaddress.StreetNumber = model.eventclientStreetNumber;
                eventclientaddress.StreetName = model.eventclientStreetName;
                eventclientaddress.Suburb = model.eventclientSuburb;
                eventclientaddress.State = model.eventclientState;
                eventclientaddress.Country = model.eventclientCountry;

                _context.Address.Add(eventclientaddress);
                _context.SaveChanges();

                //third party individual
                Individual thirdpartyIndividual = new Individual();
                thirdpartyIndividual.UserTypeID = 9;
                thirdpartyIndividual.UserFirst = model.thirdpartyUserFirst;
                thirdpartyIndividual.UserLast = model.thirdpartyUserLast;
                thirdpartyIndividual.UserMiddle = "THIRD PARTY";
                thirdpartyIndividual.UserMaiden = "THIRD PARTY";
                if (model.DateOfBirth.HasValue)
                    thirdpartyIndividual.DateOfBirth = model.clientDateOfBirth.Value;
                thirdpartyIndividual.PlaceOfBirth = "THIRD PARTY";
                thirdpartyIndividual.Gender = "THIRD PARTY";
                thirdpartyIndividual.DateAdded = System.DateTime.Now;
                thirdpartyIndividual.GuardianshipDetails = "THIRD PARTY";
                thirdpartyIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                thirdpartyIndividual.TortCaseID = relatedtortCase;
                thirdpartyIndividual.CompanionshipTypeID = 1;
                thirdpartyIndividual.PhoneNumber = model.thirdpartyPhoneNumber;
                thirdpartyIndividual.Email = model.thirdpartyEmail;
                thirdpartyIndividual.Occupation = "THIRD PARTY";
                thirdpartyIndividual.EmployerName = "THIRD PARTY";

                _context.Individual.Add(clientIndividual);
                _context.SaveChanges();
                int thirdpartyID = thirdpartyIndividual.IndividualID;

                //child individual
                Individual childIndividual = new Individual();
                childIndividual.UserTypeID = 7;
                childIndividual.UserFirst = model.childUserFirst;
                childIndividual.UserLast = model.childUserLast;
                childIndividual.UserMiddle = model.childUserMiddle;
                if (model.DateOfBirth.HasValue)
                    childIndividual.DateOfBirth = model.childDateOfBirth.Value;
                childIndividual.PlaceOfBirth = model.childPlaceOfBirth;
                childIndividual.Gender = model.childGender;
                childIndividual.DateAdded = System.DateTime.Now;
                childIndividual.GuardianshipDetails = "CHILD";
                childIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                childIndividual.TortCaseID = relatedtortCase;
                childIndividual.CompanionshipTypeID = 1;
                childIndividual.PhoneNumber = model.childPhoneNumber;
                childIndividual.Dependent = model.childDependant;
                childIndividual.Email = model.childEmail;
                childIndividual.Occupation = model.childOccupation;
                childIndividual.EmployerName = "CHILD";

                _context.Individual.Add(childIndividual);
                _context.SaveChanges();
                int childID = childIndividual.IndividualID;

                //child address's
                Address childaddress = new Address();
                childaddress.AddressTypeID = 2;
                childaddress.IndividualID = childID;
                childaddress.StreetNumber = model.childStreetNumber;
                childaddress.StreetName = model.childStreetName;
                childaddress.Suburb = model.childSuburb;
                childaddress.State = model.childState;
                childaddress.Country = model.childCountry;

                _context.Address.Add(childaddress);
                _context.SaveChanges();

                // mother individual
                Individual motherIndividual = new Individual();
                motherIndividual.UserTypeID = 14;
                motherIndividual.UserFirst = model.motherUserFirst;
                motherIndividual.UserLast = model.motherUserLast;
                motherIndividual.UserMiddle = model.motherUserMiddle;
                motherIndividual.UserMaiden = "MOTHER";
                if (model.DateOfBirth.HasValue)
                    motherIndividual.DateOfBirth = System.DateTime.Now;
                motherIndividual.PlaceOfBirth = "MOTHER";
                motherIndividual.Gender = "Female";
                motherIndividual.DateAdded = System.DateTime.Now;
                motherIndividual.GuardianshipDetails = "MOTHER";
                motherIndividual.IndividualRelationID = childID;
                //get tortcase throught the view??
                motherIndividual.TortCaseID = relatedtortCase;
                motherIndividual.CompanionshipTypeID = 2; //assume 2 (married) no option to ask about parents
                motherIndividual.Email = "MOTHER";
                motherIndividual.Occupation = "MOTHER";
                motherIndividual.EmployerName = "MOTHER";

                _context.Individual.Add(motherIndividual);
                _context.SaveChanges();
                int motherID = motherIndividual.IndividualID;

                //father individual
                Individual fatherIndividual = new Individual();
                fatherIndividual.UserTypeID = 14;
                fatherIndividual.UserFirst = model.fatherUserFirst;
                fatherIndividual.UserLast = model.fatherUserLast;
                fatherIndividual.UserMiddle = model.fatherUserMiddle;
                fatherIndividual.UserMaiden = "FATHER";
                if (model.DateOfBirth.HasValue)
                    fatherIndividual.DateOfBirth = System.DateTime.Now;
                fatherIndividual.PlaceOfBirth = "FATHER";
                fatherIndividual.Gender = "Male";
                fatherIndividual.DateAdded = System.DateTime.Now;
                fatherIndividual.GuardianshipDetails = "FATHER";
                fatherIndividual.IndividualRelationID = childID;
                //get tortcase throught the view??
                fatherIndividual.TortCaseID = relatedtortCase;
                fatherIndividual.CompanionshipTypeID = 2; //assume 2 (married) no option to ask about parents
                fatherIndividual.Email = "FATHER";
                fatherIndividual.Occupation = "FATHER";
                fatherIndividual.EmployerName = "FATHER";

                _context.Individual.Add(fatherIndividual);
                _context.SaveChanges();
                int fatherID = fatherIndividual.IndividualID;

                //victimmother individual
                Individual victimmotherIndividual = new Individual();
                victimmotherIndividual.UserTypeID = 14;
                victimmotherIndividual.UserFirst = model.victimmotherUserFirst;
                victimmotherIndividual.UserLast = model.victimmotherUserLast;
                victimmotherIndividual.UserMiddle = model.victimmotherUserMiddle;
                victimmotherIndividual.UserMaiden = model.victimmotherUserMaiden;
                if (model.DateOfBirth.HasValue)
                    victimmotherIndividual.DateOfBirth = model.victimmotherDateOfBirth.Value;
                victimmotherIndividual.PlaceOfBirth = "VICTIMMOTHER";
                victimmotherIndividual.Gender = "Female";
                victimmotherIndividual.DateAdded = System.DateTime.Now;
                victimmotherIndividual.GuardianshipDetails = "VICTIMMOTHER";
                victimmotherIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                victimmotherIndividual.TortCaseID = relatedtortCase;
                victimmotherIndividual.CompanionshipTypeID = 1;
                victimmotherIndividual.Email = model.victimmotherEmail;
                victimmotherIndividual.Dependent = model.victimmotherDependant;
                victimmotherIndividual.Occupation = "VICTIMMOTHER";
                victimmotherIndividual.EmployerName = "VICTIMMOTHER";

                _context.Individual.Add(victimmotherIndividual);
                _context.SaveChanges();
                int victimmotherID = victimmotherIndividual.IndividualID;

                //victimmother address's
                Address victimmotheraddress = new Address();
                victimmotheraddress.AddressTypeID = 2;
                victimmotheraddress.IndividualID = victimmotherID;
                victimmotheraddress.StreetNumber = model.victimmotherStreetNumber;
                victimmotheraddress.StreetName = model.victimmotherStreetName;
                victimmotheraddress.Suburb = model.victimmotherSuburb;
                victimmotheraddress.State = model.victimmotherState;
                victimmotheraddress.Country = model.victimmotherCountry;

                _context.Address.Add(victimmotheraddress);
                _context.SaveChanges();

                //victimfather individual
                Individual victimfatherIndividual = new Individual();
                victimfatherIndividual.UserTypeID = 14;
                victimfatherIndividual.UserFirst = model.victimfatherUserFirst;
                victimfatherIndividual.UserLast = model.victimfatherUserLast;
                victimfatherIndividual.UserMiddle = model.victimfatherUserMiddle;
                victimfatherIndividual.UserMaiden = model.victimfatherUserMaiden;
                if (model.DateOfBirth.HasValue)
                    victimfatherIndividual.DateOfBirth = model.victimfatherDateOfBirth.Value;
                victimfatherIndividual.PlaceOfBirth = "VICTIMFATHER";
                victimfatherIndividual.Gender = "Male";
                victimfatherIndividual.DateAdded = System.DateTime.Now;
                victimfatherIndividual.GuardianshipDetails = "VICTIMFATHER";
                victimfatherIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                victimfatherIndividual.TortCaseID = relatedtortCase;
                victimfatherIndividual.CompanionshipTypeID = 1;
                victimfatherIndividual.Email = model.victimfatherEmail;
                victimfatherIndividual.Dependent = model.victimfatherDependant;
                //victimfatherIndividual.Occupation = model.victimfatherOccupation;
                //victimfatherIndividual.EmployerName = model.victimfatherEmployerName;

                _context.Individual.Add(victimfatherIndividual);
                _context.SaveChanges();
                int victimfatherID = victimfatherIndividual.IndividualID;

                //victimfather address's
                Address victimfatheraddress = new Address();
                victimfatheraddress.AddressTypeID = 2;
                victimfatheraddress.IndividualID = victimfatherID;
                victimfatheraddress.StreetNumber = model.victimfatherStreetNumber;
                victimfatheraddress.StreetName = model.victimfatherStreetName;
                victimfatheraddress.Suburb = model.victimfatherSuburb;
                victimfatheraddress.State = model.victimfatherState;
                victimfatheraddress.Country = model.victimfatherCountry;

                _context.Address.Add(victimfatheraddress);
                _context.SaveChanges();

                //victimsibling individual
                Individual victimsiblingIndividual = new Individual();
                victimsiblingIndividual.UserTypeID = 6;
                victimsiblingIndividual.UserFirst = model.victimsiblingUserFirst;
                victimsiblingIndividual.UserLast = model.victimsiblingUserLast;
                victimsiblingIndividual.UserMiddle = model.victimsiblingUserMiddle;
                if (model.DateOfBirth.HasValue)
                    victimsiblingIndividual.DateOfBirth = model.victimsiblingDateOfBirth.Value;
                victimsiblingIndividual.PlaceOfBirth = "VICTIMSIBLING";
                victimsiblingIndividual.Gender = model.victimsiblingGender;
                victimsiblingIndividual.DateAdded = System.DateTime.Now;
                victimsiblingIndividual.GuardianshipDetails = "VICTIMSIBLING";
                victimsiblingIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                victimsiblingIndividual.TortCaseID = relatedtortCase;
                victimsiblingIndividual.CompanionshipTypeID = 1;
                victimsiblingIndividual.Email = model.victimsiblingEmail;
                victimsiblingIndividual.Dependent = model.victimsiblingDependant;
                victimsiblingIndividual.Occupation = model.victimsiblingOccupation;

                _context.Individual.Add(victimsiblingIndividual);
                _context.SaveChanges();
                int victimsiblingID = victimsiblingIndividual.IndividualID;

                //victimsibling address's
                Address victimsiblingaddress = new Address();
                victimsiblingaddress.AddressTypeID = 2;
                victimsiblingaddress.IndividualID = victimsiblingID;
                victimsiblingaddress.StreetNumber = model.victimsiblingStreetNumber;
                victimsiblingaddress.StreetName = model.victimsiblingStreetName;
                victimsiblingaddress.Suburb = model.victimsiblingSuburb;
                victimsiblingaddress.State = model.victimsiblingState;
                victimsiblingaddress.Country = model.victimsiblingCountry;

                _context.Address.Add(victimsiblingaddress);
                _context.SaveChanges();

                Payment individualPayment = new Payment();
                individualPayment.IndividualID = latestID;
                individualPayment.Received = model.Received;
                individualPayment.NumberOfPayment = model.NumberOfPayment;
                individualPayment.AmountOfPayment = model.AmountOfPayment;
                individualPayment.DocumentsSigned = model.DocumentsSigned;
                //settlement document

                _context.Payment.Add(individualPayment);
                _context.SaveChanges();

                Income individualIncome = new Income();
                individualIncome.IndividualID = latestID;
                individualIncome.MainSourceOfIncome = model.MainSourceOfIncome;
                individualIncome.AmountOfGrossIncomeOver5Years = model.AmountOfGrossIncomeOver5Years;
                individualIncome.DetailsOfIncome = model.DetailsOfIncome;
                //salary/wages document
                //tax records document
                //salary wages contracts document
                //settlement document
                if (model.CurrentFinancialYear.HasValue)
                    individualIncome.CurrentFinancialYear = model.CurrentFinancialYear.Value;
                //salary records document
                //memorial receipts document

                _context.Income.Add(individualIncome);
                _context.SaveChanges();

                PostMortemService clientVicPMS = new PostMortemService();
                clientVicPMS.IndividualID = latestID;
                if (model.DateOfService.HasValue)
                    clientVicPMS.DateOfService = model.DateOfService.Value;
                clientVicPMS.VictimDeceased = model.VictimDeceased;
                clientVicPMS.LocationOfService = model.LocationOfService;
                clientVicPMS.CostOfService = model.CostOfService;
                clientVicPMS.Memorial = model.Memorial;
                clientVicPMS.LocationOfMemorial = model.LocationOfMemorial;
                clientVicPMS.CostOfMemorial = model.CostOfMemorial;
                //funeral receipts document

                _context.PostMortemService.Add(clientVicPMS);
                _context.SaveChanges();

                MedicalHistory victimMedicalHistory = new MedicalHistory();
                victimMedicalHistory.IndividualID = latestID;
                victimMedicalHistory.MedicalConditionAtTimeOfIncident = model.MedicalConditionAtTimeOfIncident;
                //details of condition suffered by victim
                victimMedicalHistory.CurrentMedicalConditions = model.CurrentMedicalConditions;
                //details of doctor 5 years prio to event
                //did victim receive treatment (yes,no)
                victimMedicalHistory.TreatmentAfterEvent = model.TreatmentAfterEvent;
                victimMedicalHistory.HospitalizationAfterEvent = model.HospitalizationAfterEvent;
                //change post mortem conducted to bool...
                victimMedicalHistory.PostMortemConducted = model.PostMortemConducted;
                victimMedicalHistory.PostMortemLocation = model.PostMortemLocation;
                victimMedicalHistory.PostMortemConductor = model.PostMortemConductor;
                //post mortem examination document
                //medical records document
                victimMedicalHistory.WillAndTestament = model.WillAndTestament;
                //will and testament document
                victimMedicalHistory.NameOfDoctor = model.NameOfDoctor;

                _context.MedicalHistory.Add(victimMedicalHistory);
                _context.SaveChanges();

                //beneficiary individual
                Individual beneficiaryIndividual = new Individual();
                beneficiaryIndividual.UserTypeID = 11;
                beneficiaryIndividual.UserFirst = model.beneficiaryUserFirst;
                beneficiaryIndividual.UserLast = model.beneficiaryUserLast;
                beneficiaryIndividual.UserMiddle = model.beneficiaryUserMiddle;
                if (model.DateOfBirth.HasValue)
                    beneficiaryIndividual.DateOfBirth = model.beneficiaryDateOfBirth.Value;
                beneficiaryIndividual.PlaceOfBirth = "BENEFICIARY";
                beneficiaryIndividual.Gender = model.beneficiaryGender;
                beneficiaryIndividual.DateAdded = System.DateTime.Now;
                beneficiaryIndividual.GuardianshipDetails = "BENEFICIARY";
                beneficiaryIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                beneficiaryIndividual.TortCaseID = relatedtortCase;
                beneficiaryIndividual.CompanionshipTypeID = 1;
                beneficiaryIndividual.PhoneNumber = 0;
                beneficiaryIndividual.Dependent = model.beneficiaryDependant;
                beneficiaryIndividual.Email = model.beneficiaryEmail;
                beneficiaryIndividual.Occupation = model.beneficiaryOccupation;
                beneficiaryIndividual.EmployerName = "BENEFICIARY";
                //beneficiaryAge add to individual...

                _context.Individual.Add(beneficiaryIndividual);
                _context.SaveChanges();
                int beneficiaryID = beneficiaryIndividual.IndividualID;

                //beneficiary address's
                Address beneficiaryaddress = new Address();
                beneficiaryaddress.AddressTypeID = 2;
                beneficiaryaddress.IndividualID = beneficiaryID;
                beneficiaryaddress.StreetNumber = model.beneficiaryStreetNumber;
                beneficiaryaddress.StreetName = model.beneficiaryStreetName;
                beneficiaryaddress.Suburb = model.beneficiarySuburb;
                beneficiaryaddress.State = model.beneficiaryState;
                beneficiaryaddress.Country = model.beneficiaryCountry;

                _context.Address.Add(beneficiaryaddress);
                _context.SaveChanges();

                RepresentativeOfEstate individualROE = new RepresentativeOfEstate();
                individualROE.IndividualID = latestID;
                individualROE.LetterOfAdministration = model.LetterOfAdministration;
                individualROE.LocationOfLetterIssue = model.LocationOfLetterIssue;
                individualROE.CourtName = model.CourtName;
                individualROE.Contestors = model.Contestors;
                //letter of administration document

                _context.RepresentativeOfEstate.Add(individualROE);
                _context.SaveChanges();

                //ROEofestate individual
                Individual ROEIndividual = new Individual();
                ROEIndividual.UserTypeID = model.ROEUserTypeID;
                ROEIndividual.UserFirst = model.ROEUserFirst;
                ROEIndividual.UserLast = model.ROEUserLast;
                ROEIndividual.UserMiddle = model.ROEUserMiddle;
                if (model.DateOfBirth.HasValue)
                    ROEIndividual.DateOfBirth = model.ROEDateOfBirth.Value;
                ROEIndividual.PlaceOfBirth = model.ROEPlaceOfBirth;
                ROEIndividual.Gender = model.ROEGender;
                ROEIndividual.DateAdded = System.DateTime.Now;
                ROEIndividual.GuardianshipDetails = "ROE";
                ROEIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                ROEIndividual.TortCaseID = relatedtortCase;
                ROEIndividual.CompanionshipTypeID = 1;
                ROEIndividual.PhoneNumber = model.ROEPhoneNumber;
                ROEIndividual.Dependent = model.ROEDependant;
                ROEIndividual.Email = model.ROEEmail;
                ROEIndividual.Occupation = model.ROEOccupation;
                ROEIndividual.EmployerName = "ROE";
                //ROEAge add to individual...

                _context.Individual.Add(ROEIndividual);
                _context.SaveChanges();
                int ROEID = ROEIndividual.IndividualID;

                //contestorofestate individual
                Individual contestorIndividual = new Individual();
                contestorIndividual.UserTypeID = 12;
                contestorIndividual.UserFirst = model.contestorUserFirst;
                contestorIndividual.UserLast = model.contestorUserLast;
                contestorIndividual.UserMiddle = model.contestorUserMiddle;
                if (model.DateOfBirth.HasValue)
                    contestorIndividual.DateOfBirth = System.DateTime.Now;
                contestorIndividual.PlaceOfBirth = "CONTESTOR";
                contestorIndividual.Gender = model.contestorGender;
                contestorIndividual.DateAdded = System.DateTime.Now;
                contestorIndividual.GuardianshipDetails = "CONTESTOR";
                contestorIndividual.IndividualRelationID = latestID;
                //get tortcase throught the view??
                contestorIndividual.TortCaseID = relatedtortCase;
                contestorIndividual.CompanionshipTypeID = 1;
                contestorIndividual.PhoneNumber = model.contestorPhoneNumber;
                contestorIndividual.Dependent = model.contestorDependant;
                contestorIndividual.Email = model.contestorEmail;
                contestorIndividual.Occupation = model.contestorOccupation;
                contestorIndividual.EmployerName = "CONTESTOR";
                //contestorAge add to individual...

                _context.Individual.Add(contestorIndividual);
                _context.SaveChanges();
                int contestorID = contestorIndividual.IndividualID;

                VictimEducation vicEducation = new VictimEducation();
                vicEducation.IndividualID = latestID;
                vicEducation.PrimarySchool = model.PrimarySchool;
                vicEducation.SecondarySchool = model.SecondarySchool;
                vicEducation.College = model.College;
                vicEducation.TechnicalCollege = model.TechnicalCollege;
                vicEducation.University = model.University;
                vicEducation.PostgraduateStudy = model.PostgraduateStudy;
                vicEducation.OtherEducationalStudy = model.OtherEducationalStudy;
                vicEducation.ReceivedAwards = model.ReceivedAwards;
                vicEducation.NumberOfAwards = model.NumberOfAwards;
                vicEducation.DetailsOfAwards = model.DetailsOfAwards;
                //postgrad transcript document
                //college results document
                //otherstudies document
                //uni results document

                _context.VictimEducation.Add(vicEducation);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // return model, or redirect to another page....
            return View(model);
        }



        public ActionResult CaseSelector()
        {
            //var viewModel = new QuestionsViewModel();
            return View();
            //return View();
        }

        //public ActionResult Save()
        //{
        //    return RedirectToAction("New", "Questions");
        //}

        //public ActionResult Submit()
        //{

        //    return RedirectToAction("Index", "Home");
        //}

    }
}