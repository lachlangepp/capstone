﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Dtos
{
    public class CompanionshipTypeDto
    {
        public int CompanionshipTypeID { get; set; }
        public string Name { get; set; }
    }
}