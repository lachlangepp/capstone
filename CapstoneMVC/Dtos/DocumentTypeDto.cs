﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Dtos
{
    public class DocumentTypeDto
    {
        public int DocumentTypeID { get; set; }
        public string Name { get; set; }
    }
}