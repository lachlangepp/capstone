﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CapstoneMVC.Models;

namespace CapstoneMVC.Dtos
{
    public class IndividualDto
    {
        public int IndividualID { get; set; }
        public string UserFirst { get; set; }
        public string UserLast { get; set; }
        public string UserMiddle { get; set; }
        public string UserMaiden { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public bool Dependent { get; set; }
        public string EmployerName { get; set; }
        public int EmployerNumber { get; set; }
        public int PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime DateAdded { get; set; }
        public string GuardianshipDetails { get; set; }
        public bool EmailConfirmed { get; set; }

        //public Individual IndividualRelatedTo { get; set; }
        public int IndividualRelationID { get; set; }



        public TortCase TortCase { get; set; }
        //public int TortCaseID { get; set; }

        public UserType UserType { get; set; }
        //public int UserTypeID { get; set; }

        public CompanionshipType CompanionshipType { get; set; }
        //public int CompanionshipTypeID { get; set; }
    }
}