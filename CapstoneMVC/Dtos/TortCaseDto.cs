﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Dtos
{
    public class TortCaseDto
    {
        public int TortCaseID { get; set; }
        public string CaseIdentifier { get; set; }
        public string CaseName { get; set; }
        public string CaseDescription { get; set; }
        public DateTime DateAdded { get; set; }
    }
}