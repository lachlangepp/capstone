﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Dtos
{
    public class UserTypeDto
    {
        public int UserTypeID { get; set; }
        public string Name { get; set; }
    }
}