namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_individualANDneededtables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanionshipTypes",
                c => new
                    {
                        CompanionshipTypeID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CompanionshipTypeID);
            
            CreateTable(
                "dbo.Individuals",
                c => new
                    {
                        IndividualID = c.Int(nullable: false, identity: true),
                        UserFirst = c.String(),
                        UserLast = c.String(),
                        UserMiddle = c.String(),
                        UserMaiden = c.String(),
                        Gender = c.String(),
                        Occupation = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        PlaceOfBirth = c.String(),
                        Dependent = c.Boolean(nullable: false),
                        EmployerName = c.String(),
                        EmployerNumber = c.Int(nullable: false),
                        PhoneNumber = c.Int(nullable: false),
                        Email = c.String(),
                        DateAdded = c.DateTime(nullable: false),
                        GuardianshipDetails = c.String(),
                        IndividualRelationID = c.Int(nullable: false),
                        TortCaseID = c.Int(nullable: false),
                        UserTypeID = c.Int(nullable: false),
                        CompanionshipTypeID = c.Int(nullable: false),
                        IndividualRelatedTo_IndividualID = c.Int(),
                    })
                .PrimaryKey(t => t.IndividualID)
                .ForeignKey("dbo.CompanionshipTypes", t => t.CompanionshipTypeID, cascadeDelete: true)
                .ForeignKey("dbo.Individuals", t => t.IndividualRelatedTo_IndividualID)
                .ForeignKey("dbo.TortCases", t => t.TortCaseID, cascadeDelete: true)
                .ForeignKey("dbo.UserTypes", t => t.UserTypeID, cascadeDelete: true)
                .Index(t => t.TortCaseID)
                .Index(t => t.UserTypeID)
                .Index(t => t.CompanionshipTypeID)
                .Index(t => t.IndividualRelatedTo_IndividualID);
            
            CreateTable(
                "dbo.TortCases",
                c => new
                    {
                        TortCaseID = c.Int(nullable: false, identity: true),
                        CaseIdentifier = c.String(),
                        CaseName = c.String(),
                        CaseDescription = c.String(),
                        DateAdded = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TortCaseID);
            
            CreateTable(
                "dbo.UserTypes",
                c => new
                    {
                        UserTypeID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.UserTypeID);

            CreateIndex("dbo.AspNetUsers", "IndividualID");
            AddForeignKey("dbo.AspNetUsers", "IndividualID", "dbo.Individuals", "IndividualID", cascadeDelete: true);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Individuals", "UserTypeID", "dbo.UserTypes");
            DropForeignKey("dbo.Individuals", "TortCaseID", "dbo.TortCases");
            DropForeignKey("dbo.Individuals", "IndividualRelatedTo_IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.Individuals", "CompanionshipTypeID", "dbo.CompanionshipTypes");
            DropForeignKey("dbo.AspNetUsers", "IndividualID", "dbo.Individuals");
            DropIndex("dbo.Individuals", new[] { "IndividualRelatedTo_IndividualID" });
            DropIndex("dbo.Individuals", new[] { "CompanionshipTypeID" });
            DropIndex("dbo.Individuals", new[] { "UserTypeID" });
            DropIndex("dbo.Individuals", new[] { "TortCaseID" });
            DropIndex("dbo.AspNetUsers", new[] { "IndividualID" });
            DropTable("dbo.UserTypes");
            DropTable("dbo.TortCases");
            DropTable("dbo.Individuals");
            DropTable("dbo.CompanionshipTypes");
            
        }
    }
}
