namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_documentANDaddress : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressID = c.Int(nullable: false, identity: true),
                        StreetNumber = c.String(),
                        StreetName = c.String(),
                        Suburb = c.String(),
                        PostCode = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        AddressTypeID = c.Int(nullable: false),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AddressID)
                .ForeignKey("dbo.AddressTypes", t => t.AddressTypeID, cascadeDelete: true)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.AddressTypeID)
                .Index(t => t.IndividualID);
            
            CreateTable(
                "dbo.AddressTypes",
                c => new
                    {
                        AddressTypeID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.AddressTypeID);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        DocumentID = c.Int(nullable: false, identity: true),
                        DocumentFile = c.String(),
                        DocumentTypeID = c.Int(nullable: false),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DocumentID)
                .ForeignKey("dbo.DocumentTypes", t => t.DocumentTypeID, cascadeDelete: true)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.DocumentTypeID)
                .Index(t => t.IndividualID);
            
            CreateTable(
                "dbo.DocumentTypes",
                c => new
                    {
                        DocumentTypeID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.DocumentTypeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Documents", "IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.Documents", "DocumentTypeID", "dbo.DocumentTypes");
            DropForeignKey("dbo.Addresses", "IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.Addresses", "AddressTypeID", "dbo.AddressTypes");
            DropIndex("dbo.Documents", new[] { "IndividualID" });
            DropIndex("dbo.Documents", new[] { "DocumentTypeID" });
            DropIndex("dbo.Addresses", new[] { "IndividualID" });
            DropIndex("dbo.Addresses", new[] { "AddressTypeID" });
            DropTable("dbo.DocumentTypes");
            DropTable("dbo.Documents");
            DropTable("dbo.AddressTypes");
            DropTable("dbo.Addresses");
        }
    }
}
