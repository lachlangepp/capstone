namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_attorney : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attorneys",
                c => new
                    {
                        AttorneyID = c.Int(nullable: false, identity: true),
                        LicenseNumber = c.String(),
                        FirmName = c.String(),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AttorneyID)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.IndividualID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Attorneys", "IndividualID", "dbo.Individuals");
            DropIndex("dbo.Attorneys", new[] { "IndividualID" });
            DropTable("dbo.Attorneys");
        }
    }
}
