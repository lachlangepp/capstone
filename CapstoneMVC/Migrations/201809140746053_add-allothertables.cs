namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addallothertables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DefactoCompanions",
                c => new
                    {
                        DefactoCompanionID = c.String(nullable: false, maxLength: 128),
                        DateOfCommencementOfRelationship = c.DateTime(nullable: false),
                        RelationshipContinued = c.Boolean(nullable: false),
                        FacsimileNumber = c.Int(nullable: false),
                        NumberOfChildren = c.Int(nullable: false),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DefactoCompanionID)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.IndividualID);
            
            CreateTable(
                "dbo.Incomes",
                c => new
                    {
                        IncomeID = c.Int(nullable: false, identity: true),
                        MainSourceOfIncome = c.String(),
                        AmountOfGrossIncomeOver5Years = c.Int(nullable: false),
                        DetailsOfIncome = c.String(),
                        CurrentFinancialYear = c.DateTime(nullable: false),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IncomeID)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.IndividualID);
            
            CreateTable(
                "dbo.MedicalHistories",
                c => new
                    {
                        MedicalHistoryID = c.Int(nullable: false, identity: true),
                        MedicalConditionAtTimeOfIncident = c.String(),
                        CurrentMedicalConditions = c.String(),
                        NameOfDoctor = c.String(),
                        AddressOfDoctor = c.String(),
                        TreatmentAfterEvent = c.String(),
                        HospitalizationAfterEvent = c.Boolean(nullable: false),
                        PostMortemConducted = c.String(),
                        PostMortemLocation = c.String(),
                        PostMortemConductor = c.String(),
                        WillAndTestament = c.String(),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MedicalHistoryID)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.IndividualID);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentID = c.Int(nullable: false, identity: true),
                        Received = c.Boolean(nullable: false),
                        NumberOfPayment = c.Int(nullable: false),
                        AmountOfPayment = c.Int(nullable: false),
                        DocumentsSigned = c.Boolean(nullable: false),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PaymentID)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.IndividualID);
            
            CreateTable(
                "dbo.PostMortemServices",
                c => new
                    {
                        PostMortemServiceID = c.Int(nullable: false, identity: true),
                        VictimDeceased = c.Boolean(nullable: false),
                        DateOfService = c.DateTime(nullable: false),
                        LocationOfService = c.String(),
                        CostOfService = c.Int(nullable: false),
                        Memorial = c.Boolean(nullable: false),
                        LocationOfMemorial = c.String(),
                        CostOfMemorial = c.Int(nullable: false),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostMortemServiceID)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.IndividualID);
            
            CreateTable(
                "dbo.RepresentativeOfEstates",
                c => new
                    {
                        RepresentativeOfEstateID = c.Int(nullable: false, identity: true),
                        LetterOfAdministration = c.Boolean(nullable: false),
                        LocationOfLetterIssue = c.String(),
                        Contestors = c.Boolean(nullable: false),
                        CourtName = c.String(),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RepresentativeOfEstateID)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.IndividualID);
            
            CreateTable(
                "dbo.VictimEducations",
                c => new
                    {
                        VictimEducationID = c.Int(nullable: false, identity: true),
                        PrimarySchool = c.String(),
                        SecondarySchool = c.String(),
                        College = c.String(),
                        TechnicalCollege = c.String(),
                        University = c.String(),
                        PostgraduateStudy = c.String(),
                        OtherEducationalStudy = c.String(),
                        ReceivedAwards = c.Boolean(nullable: false),
                        NumberOfAwards = c.Int(nullable: false),
                        DetailsOfAwards = c.String(),
                        IndividualID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.VictimEducationID)
                .ForeignKey("dbo.Individuals", t => t.IndividualID, cascadeDelete: true)
                .Index(t => t.IndividualID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VictimEducations", "IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.RepresentativeOfEstates", "IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.PostMortemServices", "IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.Payments", "IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.MedicalHistories", "IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.Incomes", "IndividualID", "dbo.Individuals");
            DropForeignKey("dbo.DefactoCompanions", "IndividualID", "dbo.Individuals");
            DropIndex("dbo.VictimEducations", new[] { "IndividualID" });
            DropIndex("dbo.RepresentativeOfEstates", new[] { "IndividualID" });
            DropIndex("dbo.PostMortemServices", new[] { "IndividualID" });
            DropIndex("dbo.Payments", new[] { "IndividualID" });
            DropIndex("dbo.MedicalHistories", new[] { "IndividualID" });
            DropIndex("dbo.Incomes", new[] { "IndividualID" });
            DropIndex("dbo.DefactoCompanions", new[] { "IndividualID" });
            DropTable("dbo.VictimEducations");
            DropTable("dbo.RepresentativeOfEstates");
            DropTable("dbo.PostMortemServices");
            DropTable("dbo.Payments");
            DropTable("dbo.MedicalHistories");
            DropTable("dbo.Incomes");
            DropTable("dbo.DefactoCompanions");
        }
    }
}
