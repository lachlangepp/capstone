namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seed_tables : DbMigration
    {
        public override void Up()
        {
            //USER TYPES
            Sql(@"
            SET IDENTITY_INSERT UserTypes ON
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(1,'TortAdmin')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(2,'Client')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(3,'Lawyer')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(4,'Victim')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(5,'Spouse')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(6,'Sibling')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(7,'Child')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(8,'Companion')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(9,'Third_Party')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(10,'Doctor')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(11,'Beneficiary')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(12,'Contestor_Of_Estate')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(13,'Employer')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(14,'Parent')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(15,'PlaceHolder_0')
            INSERT INTO UserTypes(UserTypeID, Name) VALUES(16,'PlaceHolder_1')
            SET IDENTITY_INSERT UserTypes OFF
            ");

            //Companionship TYPES
            Sql(@"
            SET IDENTITY_INSERT CompanionshipTypes ON
            INSERT INTO CompanionshipTypes(CompanionshipTypeID, Name) VALUES(1,'Single')
            INSERT INTO CompanionshipTypes(CompanionshipTypeID, Name) VALUES(2,'Married')
            INSERT INTO CompanionshipTypes(CompanionshipTypeID, Name) VALUES(3,'Divorced')
            INSERT INTO CompanionshipTypes(CompanionshipTypeID, Name) VALUES(4,'Defacto')
            INSERT INTO CompanionshipTypes(CompanionshipTypeID, Name) VALUES(5,'Widowed/Widower')
            SET IDENTITY_INSERT CompanionshipTypes OFF
            ");

            //Address TYPES
            Sql(@"
            SET IDENTITY_INSERT AddressTypes ON
            INSERT INTO AddressTypes(AddressTypeID, Name) VALUES(1,'Postal')
            INSERT INTO AddressTypes(AddressTypeID, Name) VALUES(2,'Residential')
            INSERT INTO AddressTypes(AddressTypeID, Name) VALUES(3,'Billing')
            SET IDENTITY_INSERT AddressTypes OFF
            ");

            //Document TYPES
            Sql(@"
            SET IDENTITY_INSERT DocumentTypes ON
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(1,'Birth_Certificate')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(2,'Photograph')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(3,'Wedding_Certificate')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(4,'Settlement_Document')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(5,'Wage_Salary_Records')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(6,'Tax_records')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(7,'Wages_Salary_Contracts')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(8,'Receipts')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(9,'Post_Mortem_Examination_Copy')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(10,'Medical_Records')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(11,'Last_Will_Testament')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(12,'Letters_Of_Administration')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(13,'College_Results')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(14,'University_Results')
            INSERT INTO DocumentTypes(DocumentTypeID, Name) VALUES(15,'PostGraduate_Results')
            SET IDENTITY_INSERT DocumentTypes OFF
            ");
        }
        
        public override void Down()
        {
        }
    }
}
