namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seed_Test_Cases : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT[dbo].[TortCases] ON
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES (1, N'DefaultCase', N'CaseName', N'CaseDescription', N'2018-01-01 00:00:00')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(2, N'R02C04', N'Mechton Mechanics Global Engine Damage', N'Cars serviced by Mechton Mechanics regularly had engines that later exploded, causing several deaths and dozens of injuries between 2005 and 2012.', N'2018-12-12 00:00:00')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(3, N'R01C03', N'Brighton Shipping Sinking Incident', N'On the 25th July 2010, shipping container freight O300451 sunk off the coast of Brazil due to a repairable fault in the boat''s engines. ', N'2013-10-14 13:01:00')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(4, N'R05C04', N'Arrow Clothing Misprint', N'A misprint on the label of hundreds of maroon cardigans falsely identified the item as being made of cotton as opposed to wool, causing multiple allergic reactions.', N'2016-06-07 15:14:32')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(5, N'R06C01', N'Ramrung Phone Explosions', N'Hundreds of Ramrung smartphones, model T09+, contained batteries that exploded in varying conditions and extents within 6 months of being manufactured between 2014 and 2015.', N'2016-01-28 10:42:56')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(6, N'R01C04', N'Toyshiba Airbag Failure', N'Airbags in car model Toyshiba Mex HG501 consistently failed in cars manufactured immediately after the model''s release. ', N'2017-05-29 11:10:22')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(7, N'R02C05', N'Britz Ninco Product Contamination', N'Brelldo, a children''s craft product manufactured by Britz Ninco, were found to be contaminated with lead in batches TX01-TX05 inclusive, produced on the 30th November 2013.', N'2015-03-03 11:45:42')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(8, N'R03C03', N'CoalWool Strawberry Contamination', N'Strawberries produced on CoalWool farms between the 10th and 15th September 2017 were found to have shards of glass inside the fruit.', N'2018-02-15 13:26:34')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(9, N'R05C06', N'Virley Airlines Flight NG269 Crash', N'Flight NG269 crashed in the Amazon rainforest on the 18th December 2015 due to an engine failure detected three hours earlier.', N'2017-04-21 14:18:23')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(10, N'R04C01', N'Mogul Computers Security Breach', N'On the 10th October 2017, devices services by Mogul Computers in the previous two months were subject to a security breach which resulted in a leak of private, personal information that was subsequently posted to the internet.', N'2018-06-09 13:30:55')
                INSERT INTO[dbo].[TortCases] ([TortCaseID], [CaseIdentifier], [CaseName], [CaseDescription], [DateAdded]) VALUES(11, N'R01C03', N'DairyFarm Co Expired Milk', N'On the 21st June 2017, DairyFarm Co sold milk two weeks past expiration date to local retirement home Retreat Home, falsely claiming that it was still consumable.', N'2018-11-20 16:20:20')
            SET IDENTITY_INSERT[dbo].[TortCases] OFF
            ");
        }
        
        public override void Down()
        {
        }
    }
}
