namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seed_Test_Users_AND_Roles : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT[dbo].[Individuals] ON
            INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID]) VALUES (1, N'Admin', N'Admin', N'Admin', N'Admin', NULL, NULL, N'2018-01-01 00:00:00', NULL, 0, N'ADMIN', 0, 0, N'TortAdmin@Capstone.com', N'2018-01-01 00:00:00', NULL, 0, 1, 1, 1, NULL)
            INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID]) VALUES (2, N'Adam', N'Johnson', N'Campbell', N'Johnson', N'Male', N'Lawyer', N'1972-05-08 00:00:00', N'New York', 0, N'Lawyers', 1234, 1234, N'TortLawyer1@Capstone.com', N'2018-01-01 00:00:00', NULL, 0, 2, 3, 1, NULL)
            
            INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID]) VALUES (3, N'John', N'Jones', N'James', NULL, N'Male', N'Teacher', N'1957-09-09 12:00:00', N'Brisbane', 0, N'General State School', 1, 123456789, N'general@general.com.au', N'2018-09-18 00:00:00', NULL, 0, 2, 2, 4, 3)
            INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID]) VALUES (4, N'Mary', N'May', N'Molly', N'May', N'Female', N'Astro Physicist', N'1956-11-21 00:00:00', N'Perth', 0, N'Rockets''R''Us', 2, 987654321, N'rockets@rockets.com.au', N'2018-09-18 00:00:00', NULL, 0, 2, 8, 4, 3)
            
            SET IDENTITY_INSERT[dbo].[Individuals] OFF

            SET IDENTITY_INSERT[dbo].[Attorneys] ON
            INSERT INTO [dbo].[Attorneys] ([AttorneyID], [LicenseNumber], [FirmName], [IndividualID]) VALUES(1, N'98472', N'LawyersRus', 2)
            SET IDENTITY_INSERT[dbo].[Attorneys] OFF

            INSERT INTO [dbo].[AspNetUsers] ([Id], [IndividualID], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES(N'01c14609-32ff-43bd-bd40-87f7d34c6962', 1, N'Tortadmin@capstone.com', 1, N'AJ2ICvXFGsmamC5DoXgIHiGyDZ1lHO4Xl+hYGGPFnNNNtRg/N3EafdFf7FcvQU10sw==', N'b8b5e44c-0e02-441f-a899-35c11f1c8acf', NULL, 0, 0, NULL, 1, 0, N'Tortadmin@capstone.com')
            INSERT INTO [dbo].[AspNetUsers] ([Id], [IndividualID], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES(N'817bd915-c9dd-4360-8477-e3ad35f1fdab', 2, N'TortLawyer1@capstone.com', 1, N'ANE7j0edoFbeAfvELaaESotfBQCY1PnfB7aqC6dQsFqN4BSidiyEtowuFnjzGBidjw==', N'c3a341e5-5d58-44e7-b45a-6a5f1348cd19', NULL, 0, 0, NULL, 1, 0, N'TortLawyer1@capstone.com')
            INSERT INTO [dbo].[AspNetUsers] ([Id], [IndividualID], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES(N'f61ba527-b9c1-4fce-b564-295c27c30489', 3, N'client1@capstone.com', 1, N'AETuXTUsNCaQQa/oCXGVI6bQZSinGtudS5dVy57ciNHvcB4ZmxdncvFBLC587GFZ4w==', N'58da6f3a-2c9a-4109-a493-851714db9441', NULL, 0, 0, NULL, 1, 0, N'client1@capstone.com')

            INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES(N'65f24849-7c14-4509-a726-1afa6f0ecfa9', N'TortAdmin')
            INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES(N'463e0c72-c319-4471-9dd5-d409e254603f', N'TortClient')
            INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES(N'890b3fc1-9611-4fc8-b88a-0ad38c51086c', N'TortLawyer')

            INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES(N'f61ba527-b9c1-4fce-b564-295c27c30489', N'463e0c72-c319-4471-9dd5-d409e254603f')
            INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES(N'01c14609-32ff-43bd-bd40-87f7d34c6962', N'65f24849-7c14-4509-a726-1afa6f0ecfa9')
            INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES(N'817bd915-c9dd-4360-8477-e3ad35f1fdab', N'890b3fc1-9611-4fc8-b88a-0ad38c51086c')
            ");
        }

    public override void Down()
        {
        }
    }
}
