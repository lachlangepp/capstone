namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seed_Test_Client_Data : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT[dbo].[Addresses] ON
            INSERT INTO[dbo].[Addresses] ([AddressID], [StreetNumber], [StreetName], [Suburb], [PostCode], [State], [Country], [AddressTypeID], [IndividualID]) VALUES(1, N'400', N'Same Street', N'Yonga', N'4100', N'Queensland', N'Australia', 2, 3)
            INSERT INTO[dbo].[Addresses] ([AddressID], [StreetNumber], [StreetName], [Suburb], [PostCode], [State], [Country], [AddressTypeID], [IndividualID]) VALUES(2, N'10', N'Post Street', N'Bronx', N'4050', N'Queensland', N'Australia', 1, 3)
            SET IDENTITY_INSERT[dbo].[Addresses] OFF

            INSERT INTO[dbo].[DefactoCompanions] ([DefactoCompanionID], [DateOfCommencementOfRelationship], [RelationshipContinued], [FacsimileNumber], [NumberOfChildren], [IndividualID]) VALUES(N'1', N'2013-05-13 00:00:00', 1, 1234567890, 1, 3)

            SET IDENTITY_INSERT[dbo].[Incomes] ON
            INSERT INTO[dbo].[Incomes] ([IncomeID], [MainSourceOfIncome], [AmountOfGrossIncomeOver5Years], [DetailsOfIncome], [CurrentFinancialYear], [IndividualID]) VALUES(1, N'Teacher at General State School', 500000, N'For financial years 2012-2017', N'2018-07-01 00:00:00', 3)
            SET IDENTITY_INSERT[dbo].[Incomes] OFF
            
            SET IDENTITY_INSERT[dbo].[MedicalHistories] ON
            INSERT INTO[dbo].[MedicalHistories] ([MedicalHistoryID], [MedicalConditionAtTimeOfIncident], [CurrentMedicalConditions], [NameOfDoctor], [AddressOfDoctor], [TreatmentAfterEvent], [HospitalizationAfterEvent], [PostMortemConducted], [PostMortemLocation], [PostMortemConductor], [WillAndTestament], [IndividualID]) VALUES(1, N'Asthma', N'Asthma', N'Jane Newport', N'132 Hospital Street', N'Second degree burns', 1, NULL, NULL, NULL, NULL, 3)
            SET IDENTITY_INSERT[dbo].[MedicalHistories] OFF

            SET IDENTITY_INSERT[dbo].[Payments] ON
            INSERT INTO[dbo].[Payments] ([PaymentID], [Received], [NumberOfPayment], [AmountOfPayment], [DocumentsSigned], [IndividualID]) VALUES(1, 1, 1, 500, 0, 3)
            SET IDENTITY_INSERT[dbo].[Payments] OFF

            SET IDENTITY_INSERT[dbo].[VictimEducations] ON
            INSERT INTO[dbo].[VictimEducations] ([VictimEducationID], [PrimarySchool], [SecondarySchool], [College], [TechnicalCollege], [University], [PostgraduateStudy], [OtherEducationalStudy], [ReceivedAwards], [NumberOfAwards], [DetailsOfAwards], [IndividualID]) VALUES(1, N'First State School', N'Second State School', NULL, NULL, N'Third University of Technology', N'Fourth Univeristy of Technology', NULL, 1, 1, N'Bachelor of Education (Primary)', 3)
            SET IDENTITY_INSERT[dbo].[VictimEducations] OFF
            ");
        }
        
        public override void Down()
        {
        }
    }
}
