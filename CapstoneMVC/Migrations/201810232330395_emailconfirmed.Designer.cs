// <auto-generated />
namespace CapstoneMVC.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class emailconfirmed : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(emailconfirmed));
        
        string IMigrationMetadata.Id
        {
            get { return "201810232330395_emailconfirmed"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
