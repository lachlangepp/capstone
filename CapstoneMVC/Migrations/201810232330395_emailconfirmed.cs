namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class emailconfirmed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Individuals", "EmailConfirmed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Individuals", "EmailConfirmed");
        }
    }
}
