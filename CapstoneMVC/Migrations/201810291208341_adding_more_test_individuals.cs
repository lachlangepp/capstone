namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class adding_more_test_individuals : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                SET IDENTITY_INSERT [dbo].[Individuals] ON
                    INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID], [EmailConfirmed]) VALUES (7, N'Bethany', N'Mills', N'Mary', N'Hudson', N'Female', N'Dentist', N'1972-11-01 12:00:00', N'Brisbane', 1, N'Hillsbury Dentistry', 1, 123456789, N'dentist@dentist.com.au', N'2018-10-14 00:00:00', NULL, 0, 1, 2, 1, 7, 0)
                    INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID], [EmailConfirmed]) VALUES (8, N'Jemma', N'Lockyer', N'Valley', NULL, N'Female', N'Journalist', N'1981-03-18 00:00:00', N'Sydney', 0, N'The Tort Times', 192837465, 123456789, N'journalism@journalism.com', N'2018-10-14 00:00:00', NULL, 0, 1, 2, 4, 8, 0)                
                    INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID], [EmailConfirmed]) VALUES (9, N'Bob', N'Rogerson', N'Mark', NULL, N'Male', N'Waiter', N'1993-05-29 00:00:00', N'Brisbane', 0, N'Burgers Are Better', 019283476, 123456789, N'waiter@waiter.com.au', N'2018-10-14 00:00:00', NULL, 0, 1, 2, 1, 9, 0)      
                    INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID], [EmailConfirmed]) VALUES (10, N'Michael', N'Hill', N'Mark', NULL, N'Male', N'Retail Assistant', N'1990-10-03 00:00:00', N'Sydney', 1, N'Generic Jewels', 1122334455, 123456789, N'jewels@jewels.com.au', N'2018-10-14 00:00:00', NULL, 0, 1, 2, 1, 10, 0)
                    INSERT INTO [dbo].[Individuals] ([IndividualID], [UserFirst], [UserLast], [UserMiddle], [UserMaiden], [Gender], [Occupation], [DateOfBirth], [PlaceOfBirth], [Dependent], [EmployerName], [EmployerNumber], [PhoneNumber], [Email], [DateAdded], [GuardianshipDetails], [IndividualRelationID], [TortCaseID], [UserTypeID], [CompanionshipTypeID], [IndividualRelatedTo_IndividualID], [EmailConfirmed]) VALUES (11, N'Stephanie', N'Cox', N'Bianca', N'Anderson', N'Female', N'Actress', N'1985-02-14 00:00:00', N'Brisbane', 1, N'La Thome Theatre Company', 13255, 4539, N'actress@actress.com.au', N'2018-10-14 00:00:00', NULL, 0, 3, 2, 3, 11, 0)
                SET IDENTITY_INSERT [dbo].[Individuals] OFF
            ");
        }

        public override void Down()
        {
        }
    }
}
