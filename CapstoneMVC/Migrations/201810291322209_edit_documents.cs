namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class edit_documents : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "DocumentContent", c => c.Binary());
            AddColumn("dbo.Documents", "DocumentContentType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Documents", "DocumentContentType");
            DropColumn("dbo.Documents", "DocumentContent");
        }
    }
}
