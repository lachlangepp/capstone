namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class edit_documents2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "DocumentName", c => c.String());
            DropColumn("dbo.Documents", "DocumentFile");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Documents", "DocumentFile", c => c.String());
            DropColumn("dbo.Documents", "DocumentName");
        }
    }
}
