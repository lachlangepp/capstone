namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetocurrent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileModels", "IndividualID", c => c.Int(nullable: false));
            CreateIndex("dbo.FileModels", "IndividualID");
            AddForeignKey("dbo.FileModels", "IndividualID", "dbo.Individuals", "IndividualID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FileModels", "IndividualID", "dbo.Individuals");
            DropIndex("dbo.FileModels", new[] { "IndividualID" });
            DropColumn("dbo.FileModels", "IndividualID");
        }
    }
}
