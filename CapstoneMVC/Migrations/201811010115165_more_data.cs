namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class more_data : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT[dbo].[Addresses] ON
            INSERT INTO [dbo].[Addresses] ([AddressID], [StreetNumber], [StreetName], [Suburb], [PostCode], [State], [Country], [AddressTypeID], [IndividualID]) VALUES (3, N'123', N'Honest Street', N'Billbo', N'4065', N'Queensland', N'Australia', 2, 7)
            INSERT INTO [dbo].[Addresses] ([AddressID], [StreetNumber], [StreetName], [Suburb], [PostCode], [State], [Country], [AddressTypeID], [IndividualID]) VALUES (4, N'54', N'Next Road', N'Faultu', N'4321', N'Queensland', N'Australia', 1, 8)
            INSERT INTO [dbo].[Addresses] ([AddressID], [StreetNumber], [StreetName], [Suburb], [PostCode], [State], [Country], [AddressTypeID], [IndividualID]) VALUES (5, N'9', N'Honest Street', N'Billbo', N'4065', N'Queensland', N'Australia', 2, 9)
            INSERT INTO [dbo].[Addresses] ([AddressID], [StreetNumber], [StreetName], [Suburb], [PostCode], [State], [Country], [AddressTypeID], [IndividualID]) VALUES (6, N'340', N'Albert Lane', N'Bonnerley', N'2094', N'New South Wales', N'Australia', 1, 10)
            INSERT INTO [dbo].[Addresses] ([AddressID], [StreetNumber], [StreetName], [Suburb], [PostCode], [State], [Country], [AddressTypeID], [IndividualID]) VALUES (7, N'200', N'Victoria Road', N'Sangsmee', N'2030', N'New South Wales', N'Australia', 1, 11)            
            SET IDENTITY_INSERT[dbo].[Addresses] OFF
            ");
        }
        public override void Down()
        {
        }
    }
}
