namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class more_data2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.DefactoCompanions");
            AlterColumn("dbo.DefactoCompanions", "DefactoCompanionID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.DefactoCompanions", "DefactoCompanionID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.DefactoCompanions");
            AlterColumn("dbo.DefactoCompanions", "DefactoCompanionID", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.DefactoCompanions", "DefactoCompanionID");
        }
    }
}
