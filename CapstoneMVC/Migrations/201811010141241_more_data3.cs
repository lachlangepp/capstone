namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class more_data3 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT[dbo].[Incomes] ON
            INSERT INTO[dbo].[Incomes]
        ([IncomeID], [MainSourceOfIncome], [AmountOfGrossIncomeOver5Years], [DetailsOfIncome], [CurrentFinancialYear], [IndividualID]) VALUES(2, N'Dentist at Hillsbury Dentistry', 820000, N'For financial years 2012-2017', N'2018-07-01 00:00:00', 7)
            INSERT INTO[dbo].[Incomes]
        ([IncomeID], [MainSourceOfIncome], [AmountOfGrossIncomeOver5Years], [DetailsOfIncome], [CurrentFinancialYear], [IndividualID]) VALUES(3, N'Journalist at The Tort Times', 70000, N'For financial years 2012-2017', N'2018-07-01 00:00:00', 8)
            INSERT INTO[dbo].[Incomes]
        ([IncomeID], [MainSourceOfIncome], [AmountOfGrossIncomeOver5Years], [DetailsOfIncome], [CurrentFinancialYear], [IndividualID]) VALUES(4, N'Waiter at Burgers Are Better', 40000, N'For financial years 2012-2017', N'2018-07-01 00:00:00', 9)
            INSERT INTO[dbo].[Incomes]
        ([IncomeID], [MainSourceOfIncome], [AmountOfGrossIncomeOver5Years], [DetailsOfIncome], [CurrentFinancialYear], [IndividualID]) VALUES(5, N'Retail Assistant at Generic Jewels', 60000, N'For financial years 2012-2017', N'2018-07-01 00:00:00', 10)
            INSERT INTO[dbo].[Incomes]
        ([IncomeID], [MainSourceOfIncome], [AmountOfGrossIncomeOver5Years], [DetailsOfIncome], [CurrentFinancialYear], [IndividualID]) VALUES(6, N'Actress at La Thome Theatre Company', 20000, N'For financial years 2012-2017', N'2018-07-01 00:00:00', 11)
            SET IDENTITY_INSERT[dbo].[Incomes] OFF
            ");
    }
        
        public override void Down()
        {
        }
    }
}
