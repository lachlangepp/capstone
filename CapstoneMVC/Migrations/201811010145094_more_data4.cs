namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class more_data4 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                SET IDENTITY_INSERT[dbo].[MedicalHistories] ON
           INSERT INTO[dbo].[MedicalHistories]
        ([MedicalHistoryID], [MedicalConditionAtTimeOfIncident], [CurrentMedicalConditions], [NameOfDoctor], [AddressOfDoctor], [TreatmentAfterEvent], [HospitalizationAfterEvent], [PostMortemConducted], [PostMortemLocation], [PostMortemConductor], [WillAndTestament], [IndividualID]) VALUES(2, NULL, N'Anxiety Disorder', N'Jane Newport', N'132 Hospital Street', N'PTSD Treatment', 1, NULL, NULL, NULL, NULL, 7)
            INSERT INTO[dbo].[MedicalHistories]
        ([MedicalHistoryID], [MedicalConditionAtTimeOfIncident], [CurrentMedicalConditions], [NameOfDoctor], [AddressOfDoctor], [TreatmentAfterEvent], [HospitalizationAfterEvent], [PostMortemConducted], [PostMortemLocation], [PostMortemConductor], [WillAndTestament], [IndividualID]) VALUES(3, NULL, NULL, N'Thomas Anderson', N'54 Ward Road', N'Third degree burns, minor lacerations', 1, NULL, NULL, NULL, NULL, 8)
            INSERT INTO[dbo].[MedicalHistories]
        ([MedicalHistoryID], [MedicalConditionAtTimeOfIncident], [CurrentMedicalConditions], [NameOfDoctor], [AddressOfDoctor], [TreatmentAfterEvent], [HospitalizationAfterEvent], [PostMortemConducted], [PostMortemLocation], [PostMortemConductor], [WillAndTestament], [IndividualID]) VALUES(4, N'Diabetes', N'Diabetes', N'Willemina Struth', N'42 Council Road', N'Low blood pressure treatment', 1, NULL, NULL, NULL, NULL, 9)
            INSERT INTO[dbo].[MedicalHistories]
        ([MedicalHistoryID], [MedicalConditionAtTimeOfIncident], [CurrentMedicalConditions], [NameOfDoctor], [AddressOfDoctor], [TreatmentAfterEvent], [HospitalizationAfterEvent], [PostMortemConducted], [PostMortemLocation], [PostMortemConductor], [WillAndTestament], [IndividualID]) VALUES(5, NULL, NULL, N'Jane Newport', N'132 Hospital Street', N'PTSD Treatment', 1, NULL, NULL, NULL, NULL, 10)
            INSERT INTO[dbo].[MedicalHistories]
        ([MedicalHistoryID], [MedicalConditionAtTimeOfIncident], [CurrentMedicalConditions], [NameOfDoctor], [AddressOfDoctor], [TreatmentAfterEvent], [HospitalizationAfterEvent], [PostMortemConducted], [PostMortemLocation], [PostMortemConductor], [WillAndTestament], [IndividualID]) VALUES(6, N'Asthma', N'Asthma, Anxiety Disorder', N'Willemina Struth', N'42 Council Road', NULL, 1, NULL, NULL, NULL, NULL, 11)
            SET IDENTITY_INSERT[dbo].[MedicalHistories] OFF
");
    }
        
        public override void Down()
        {
        }
    }
}
