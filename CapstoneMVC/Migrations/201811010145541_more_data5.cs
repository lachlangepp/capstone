namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class more_data5 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT[dbo].[Payments]
        ON
            INSERT INTO[dbo].[Payments]
        ([PaymentID], [Received], [NumberOfPayment], [AmountOfPayment], [DocumentsSigned], [IndividualID]) VALUES(2, 1, 1, 2500, 0, 7)
            INSERT INTO[dbo].[Payments]
        ([PaymentID], [Received], [NumberOfPayment], [AmountOfPayment], [DocumentsSigned], [IndividualID]) VALUES(3, 1, 1, 700, 0, 9)
            INSERT INTO[dbo].[Payments]
        ([PaymentID], [Received], [NumberOfPayment], [AmountOfPayment], [DocumentsSigned], [IndividualID]) VALUES(4, 2, 1, 250, 1, 10)
            INSERT INTO[dbo].[Payments]
        ([PaymentID], [Received], [NumberOfPayment], [AmountOfPayment], [DocumentsSigned], [IndividualID]) VALUES(5, 2, 2, 250, 1, 10)
            SET IDENTITY_INSERT[dbo].[Payments] OFF
");
    }
        
        public override void Down()
        {
        }
    }
}
