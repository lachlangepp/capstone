namespace CapstoneMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class more_data6 : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT[dbo].[VictimEducations] ON
            INSERT INTO [dbo].[VictimEducations] ([VictimEducationID], [PrimarySchool], [SecondarySchool], [College], [TechnicalCollege], [University], [PostgraduateStudy], [OtherEducationalStudy], [ReceivedAwards], [NumberOfAwards], [DetailsOfAwards], [IndividualID]) VALUES (2, N'First State School', N'Second State School', N'Third College', NULL, NULL, NULL, NULL, 1, 1, N'Bachelor of Vet Science', 7)
            INSERT INTO [dbo].[VictimEducations] ([VictimEducationID], [PrimarySchool], [SecondarySchool], [College], [TechnicalCollege], [University], [PostgraduateStudy], [OtherEducationalStudy], [ReceivedAwards], [NumberOfAwards], [DetailsOfAwards], [IndividualID]) VALUES (3, N'Another State School', N'Next State School', NULL, N'Again Technical College', NULL, N'Fourth Univeristy of Technology', NULL, 1, 1, N'Certificate II (Hospitality)', 8)
            INSERT INTO [dbo].[VictimEducations] ([VictimEducationID], [PrimarySchool], [SecondarySchool], [College], [TechnicalCollege], [University], [PostgraduateStudy], [OtherEducationalStudy], [ReceivedAwards], [NumberOfAwards], [DetailsOfAwards], [IndividualID]) VALUES (4, N'First State School', N'Second State School', NULL, NULL, N'Third University of Technology', N'Fourth Univeristy of Technology', NULL, 1, 1, N'Bachelor of Business', 9)
            INSERT INTO [dbo].[VictimEducations] ([VictimEducationID], [PrimarySchool], [SecondarySchool], [College], [TechnicalCollege], [University], [PostgraduateStudy], [OtherEducationalStudy], [ReceivedAwards], [NumberOfAwards], [DetailsOfAwards], [IndividualID]) VALUES (5, N'Yetter State School', N'Red Robin College', NULL, NULL, N'Third University of Technology', NULL, NULL, 1, 1, N'Bachelor of Information Technology (Computer Architecture)', 10)
            INSERT INTO [dbo].[VictimEducations] ([VictimEducationID], [PrimarySchool], [SecondarySchool], [College], [TechnicalCollege], [University], [PostgraduateStudy], [OtherEducationalStudy], [ReceivedAwards], [NumberOfAwards], [DetailsOfAwards], [IndividualID]) VALUES (6, N'Another State School', N'Second State School', NULL, NULL, N'Third University of Artistry', N'Fourth Univeristy of Humanities', NULL, 1, 1, N'Bachelor of Social Sciences (Psychology)', 11)
            SET IDENTITY_INSERT[dbo].[VictimEducations] OFF
            ");
        }
        
        public override void Down()
        {
        }
    }
}
