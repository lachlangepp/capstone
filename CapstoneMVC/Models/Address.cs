﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Models
{
    public class Address
    {
        [Key]
        public int AddressID { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public AddressType AddressType { get; set; }
        [ForeignKey("AddressType")]
        public int AddressTypeID { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }
    }
}