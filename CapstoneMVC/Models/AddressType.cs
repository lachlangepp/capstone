﻿using System.ComponentModel.DataAnnotations;

namespace CapstoneMVC.Models
{
    public class AddressType
    {
        [Key]
        public int AddressTypeID { get; set; }
        public string Name { get; set; }
    }
}