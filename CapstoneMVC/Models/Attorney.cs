﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Models
{
    public class Attorney
    { 
        [Key]
        public int AttorneyID { get; set; }
        public string LicenseNumber { get; set; }
        public string FirmName { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }
    }
}