using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapstoneMVC.Models
{
    using System;
    
    public  class DefactoCompanion
    {
        [Key]
        public int DefactoCompanionID { get; set; }
        public DateTime DateOfCommencementOfRelationship { get; set; }
        public bool RelationshipContinued { get; set; }
        public int FacsimileNumber { get; set; }
        public int NumberOfChildren { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }
    }
}
