﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Models
{
    public class Document
    {
        [Key]
        public int DocumentID { get; set; }

        [Display(Name = "Uploaded File")]
        public string DocumentName { get; set; }

        public byte[] DocumentContent { get; set; }

        public string DocumentContentType { get; set; }

        public DocumentType DocumentType { get; set; }
        [ForeignKey("DocumentType")]
        public int DocumentTypeID { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }
    }
}