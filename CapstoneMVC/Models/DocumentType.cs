﻿using System.ComponentModel.DataAnnotations;

namespace CapstoneMVC.Models
{
    public class DocumentType
    {
        [Key]
        public int DocumentTypeID { get; set; }
        public string Name { get; set; }
    }
}