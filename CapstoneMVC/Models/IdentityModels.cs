﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CapstoneMVC.Models;

namespace CapstoneMVC.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public int IndividualID { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        //    ####    add DBsets here to create tables    ####

        public DbSet<Address> Address { get; set; }
        public DbSet<Attorney> Attorney { get; set; }
        public DbSet<DefactoCompanion> DefactoCompanion { get; set; }
        public DbSet<Document> Document { get; set; }
        public DbSet<FileModel> File { get; set; }
        public DbSet<Income> Income { get; set; }
        public DbSet<Individual> Individual { get; set; }
        public DbSet<MedicalHistory> MedicalHistory { get; set; }
        public DbSet<Payment> Payment { get; set; }
        public DbSet<PostMortemService> PostMortemService { get; set; }
        public DbSet<RepresentativeOfEstate> RepresentativeOfEstate { get; set; }
        public DbSet<TortCase> TortCase { get; set; }
        public DbSet<UserType> UserType { get; set; }
        public DbSet<AddressType> AddressType { get; set; }
        public DbSet<CompanionshipType> CompanionshipType { get; set; }
        public DbSet<DocumentType> DocumentType { get; set; }
        public DbSet<VictimEducation> VictimEducation { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}