﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Models
{
    public class Income
    {
        [Key]
        public int IncomeID { get; set; }
        public string MainSourceOfIncome { get; set; }
        public int AmountOfGrossIncomeOver5Years { get; set; }
        public string DetailsOfIncome { get; set; }
        public DateTime CurrentFinancialYear { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }




    }
}