﻿using CapstoneMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Server;

namespace CapstoneMVC.Models
{
    public class Individual
    {
        [Key]
        public int IndividualID { get; set; }
        public string UserFirst { get; set; }
        public string UserLast { get; set; }
        public string UserMiddle { get; set; }
        public string UserMaiden { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public bool Dependent { get; set; }
        public string EmployerName { get; set; }
        public int EmployerNumber { get; set; }
        public int PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime DateAdded { get; set; }
        public string GuardianshipDetails { get; set; }


        public Individual IndividualRelatedTo { get; set; }
        //[ForeignKey("Individual")]
        // How to add this key link to itself...???
        public int IndividualRelationID { get; set; }
        


        public TortCase TortCase { get; set; }
        [ForeignKey("TortCase")]
        public int TortCaseID { get; set; }

        public UserType UserType { get; set; }
        [ForeignKey("UserType")]
        public int UserTypeID { get; set; }

        public CompanionshipType CompanionshipType { get; set; }
        [ForeignKey("CompanionshipType")]
        public int CompanionshipTypeID { get; set; }

        // custom attributes
        public bool EmailConfirmed { get; set; }
    }
}