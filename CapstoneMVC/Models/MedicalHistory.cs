using System.ComponentModel.DataAnnotations.Schema;

namespace CapstoneMVC.Models
{
    using System.ComponentModel.DataAnnotations;

    public class MedicalHistory
    {
        [Key]
        public int MedicalHistoryID { get; set; }
        public string MedicalConditionAtTimeOfIncident { get; set; }
        public string CurrentMedicalConditions { get; set; }
        public string NameOfDoctor { get; set; }
        public string AddressOfDoctor { get; set; }
        public string TreatmentAfterEvent { get; set; }
        public bool HospitalizationAfterEvent { get; set; }
        public string PostMortemConducted { get; set; }
        public string PostMortemLocation { get; set; }
        public string PostMortemConductor { get; set; }
        public string WillAndTestament { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }


    }
}
