using System.ComponentModel.DataAnnotations.Schema;

namespace CapstoneMVC.Models
{
    using System.ComponentModel.DataAnnotations;

    public class Payment
    {
        [Key]
        public int PaymentID { get; set; }
        public bool Received { get; set; }
        public int NumberOfPayment { get; set; }
        public int AmountOfPayment { get; set; }
        public bool DocumentsSigned { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }

    }
}
