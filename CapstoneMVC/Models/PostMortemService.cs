using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapstoneMVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public class PostMortemService
    {
        [Key]
        public int PostMortemServiceID { get; set; }
        public bool VictimDeceased { get; set; }
        public DateTime DateOfService { get; set; }
        public string LocationOfService { get; set; }
        public int CostOfService { get; set; }
        public bool Memorial { get; set; }
        public string LocationOfMemorial { get; set; }
        public int CostOfMemorial { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }
    }
}
