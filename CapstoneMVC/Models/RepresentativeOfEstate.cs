using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapstoneMVC.Models
{
    
    public class RepresentativeOfEstate
    {
        [Key]
        public int RepresentativeOfEstateID { get; set; }
        public bool LetterOfAdministration { get; set; }
        public string LocationOfLetterIssue { get; set; }
        public bool Contestors { get; set; }
        public string CourtName { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }

    }
}
