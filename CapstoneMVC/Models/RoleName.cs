﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneMVC.Models
{
    public static class RoleName
    {
        public const string TortAdmin = "TortAdmin";
        public const string TortLawyer = "TortLawyer";
        public const string TortClient = "Client";
        public const string TortAdminandLawyer = TortAdmin + "," + TortLawyer;
    }
}