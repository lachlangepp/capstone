﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CapstoneMVC.Models
{
    public class TortCase
    {
        [Key]
        public int TortCaseID { get; set; }
        public string CaseIdentifier { get; set; }
        public string CaseName { get; set; }
        public string CaseDescription { get; set; }
        public DateTime DateAdded { get; set; }
    }
}