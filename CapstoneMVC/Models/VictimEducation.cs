using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapstoneMVC.Models
{
    
    public class VictimEducation
    {
        [Key]
        public int VictimEducationID { get; set; }
        public string PrimarySchool { get; set; }
        public string SecondarySchool { get; set; }
        public string College { get; set; }
        public string TechnicalCollege { get; set; }
        public string University { get; set; }
        public string PostgraduateStudy { get; set; }
        public string OtherEducationalStudy { get; set; }
        public bool ReceivedAwards { get; set; }
        public int NumberOfAwards { get; set; }
        public string DetailsOfAwards { get; set; }

        public Individual Individual { get; set; }
        [ForeignKey("Individual")]
        public int IndividualID { get; set; }

    }
}
