﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CapstoneMVC.Startup))]
namespace CapstoneMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
