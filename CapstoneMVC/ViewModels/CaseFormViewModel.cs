﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CapstoneMVC.Models;

namespace CapstoneMVC.ViewModels
{
    public class CaseFormViewModel
    {
        public int TortCaseID { get; set; }

        [Display(Name = "Case Identifier")]
        [Required]
        public string CaseIdentifier { get; set; }

        [Display(Name = "Case Name")]
        [Required]
        public string CaseName { get; set; }

        [Display(Name = "Case Description")]
        [Required]
        public string CaseDescription { get; set; }
        public DateTime DateAdded { get; set; }
        public string Title => TortCaseID != 0 ? "Edit Case" : "New Case";

        public CaseFormViewModel()
        {
            TortCaseID = 0;
        }

        public CaseFormViewModel(TortCase tortCase)
        {
            TortCaseID = tortCase.TortCaseID;
            CaseName = tortCase.CaseName;
            CaseIdentifier = tortCase.CaseIdentifier;
            CaseDescription = tortCase.CaseDescription;
            DateAdded = tortCase.DateAdded;
        }
    }
}