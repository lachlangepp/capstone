﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CapstoneMVC.Models;

namespace CapstoneMVC.ViewModels
{
    public class IndividualFormViewModel
    {
        public string Title => IndividualID != 0 ? "Edit Individual" : "New Individual";

        public IndividualFormViewModel()
        {
            IndividualID = 0;
        }
        [Key]
        public int IndividualID { get; set; }
        public string UserFirst { get; set; }
        public string UserLast { get; set; }
        public string UserMiddle { get; set; }
        public string UserMaiden { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public bool Dependent { get; set; }
        public string EmployerName { get; set; }
        public int EmployerNumber { get; set; }
        public int PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime DateAdded { get; set; }
        public string GuardianshipDetails { get; set; }


        public Individual IndividualRelatedTo { get; set; }
        //[ForeignKey("Individual")]
        // How to add this key link to itself...???
        public int IndividualRelationID { get; set; }

        public TortCase TortCase { get; set; }
        public int TortCaseID { get; set; }

        public UserType UserType { get; set; }
        public int UserTypeID { get; set; }

        public CompanionshipType CompanionshipType { get; set; }
        public int CompanionshipTypeID { get; set; }

        public IndividualFormViewModel(Individual individual)
        {
            IndividualID = individual.IndividualID;
            UserFirst = individual.UserFirst;
            UserLast = individual.UserLast;
            UserMiddle = individual.UserMiddle;
            UserMaiden = individual.UserMaiden;
            Gender = individual.Gender;
            Occupation = individual.Occupation;
            DateOfBirth = individual.DateOfBirth;
            PlaceOfBirth = individual.PlaceOfBirth;
            Dependent = individual.Dependent;
            EmployerName = individual.EmployerName;
            EmployerNumber = individual.EmployerNumber;
            PhoneNumber = individual.PhoneNumber;
            Email = individual.Email;
            DateAdded = individual.DateAdded;
            GuardianshipDetails = individual.GuardianshipDetails;
            IndividualRelatedTo = individual.IndividualRelatedTo;
            IndividualRelationID = individual.IndividualRelationID;
            TortCase = individual.TortCase;
            TortCaseID = individual.TortCaseID;
            UserType = individual.UserType;
            UserTypeID = individual.UserTypeID;
            CompanionshipType = individual.CompanionshipType;
            CompanionshipTypeID = individual.CompanionshipTypeID;
            EmailConfirmed = individual.EmailConfirmed;
        }

        //Custom Attributes
        // all other attributes from other models go here for the questionscontroll to be able to access them.

        public bool EmailConfirmed { get; set; }


        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }
}