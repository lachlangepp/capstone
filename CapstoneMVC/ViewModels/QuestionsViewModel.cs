﻿using CapstoneMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneMVC.ViewModels
{
    public class QuestionsViewModel
    {
        // from individual table
        public string UserFirst { get; set; }
        public string UserLast { get; set; }
        public string UserMiddle { get; set; }
        public string UserMaiden { get; set; }

        //spouse name
        public string spouseName { get; set; }

        //spouse late deets
        public string spouseUserFirst { get; set; }
        public string spouseUserLast { get; set; }
        public string spouseUserMiddle { get; set; }
        public string spouseUserMaiden { get; set; }
        public int spousePhoneNumber { get; set; }
        public string spouseEmail { get; set; }
        public int spouseFacsimile { get; set; }
        public string spouseOccupation { get; set; }
        public Nullable<System.DateTime> spouseDateofDeath { get; set; }


        //companion deets
        public string compUserFirst { get; set; }
        public string compUserLast { get; set; }
        public string compUserMiddle { get; set; }
        public string compUserMaiden { get; set; }
        public int compPhoneNumber { get; set; }
        public string compEmail { get; set; }
        public int compFacsimile { get; set; }
        public string compOccupation { get; set; }
        public string compEmployerName { get; set; }

        //client deets
        public string clientUserFirst { get; set; }
        public string clientUserLast { get; set; }
        public string clientUserMiddle { get; set; }
        public string clientUserMaiden { get; set; }
        public int clientBusinessPhoneNumber { get; set; }
        public int clientCellPhoneNumber { get; set; }
        public int clientHomePhoneNumber { get; set; }
        public Nullable<System.DateTime> clientDateOfBirth { get; set; }
        public string clientPlaceOfBirth { get; set; }
        public string clientGender { get; set; }
        public int clientvicUserTypeID { get; set; }


        //thirdparty deets
        public string thirdpartyUserFirst { get; set; }
        public string thirdpartyUserLast { get; set; }
        public string thirdpartyEmail { get; set; }
        public int thirdpartyPhoneNumber { get; set; }

        //child deets
        public string childUserFirst { get; set; }
        public string childUserLast { get; set; }
        public string childUserMiddle { get; set; }
        public int childPhoneNumber { get; set; }
        public Nullable<System.DateTime> childDateOfBirth { get; set; }
        public string childPlaceOfBirth { get; set; }
        public string childGender { get; set; }
        public string childEmail { get; set; }
        public bool childDependant { get; set; }
        public string childOccupation { get; set; }

        //mother deets
        public string motherUserFirst { get; set; }
        public string motherUserLast { get; set; }
        public string motherUserMiddle { get; set; }
        

        //father deets
        public string fatherUserFirst { get; set; }
        public string fatherUserLast { get; set; }
        public string fatherUserMiddle { get; set; }
        

        // victimfather deets
        public string victimfatherUserFirst { get; set; }
        public string victimfatherUserLast { get; set; }
        public string victimfatherUserMiddle { get; set; }
        public string victimfatherUserMaiden { get; set; }
        public int victimfatherPhoneNumber { get; set; }
        public string victimfatherEmail { get; set; }
        public bool victimfatherDependant { get; set; }
        public Nullable<System.DateTime> victimfatherDateOfBirth { get; set; }

        // victimmother deets
        public string victimmotherUserFirst { get; set; }
        public string victimmotherUserLast { get; set; }
        public string victimmotherUserMiddle { get; set; }
        public string victimmotherUserMaiden { get; set; }
        public int victimmotherPhoneNumber { get; set; }
        public string victimmotherEmail { get; set; }
        public bool victimmotherDependant { get; set; }
        public Nullable<System.DateTime> victimmotherDateOfBirth { get; set; }

        // victimsibling deets
        public string victimsiblingUserFirst { get; set; }
        public string victimsiblingUserLast { get; set; }
        public string victimsiblingUserMiddle { get; set; }
        public int victimsiblingPhoneNumber { get; set; }
        public string victimsiblingEmail { get; set; }
        public bool victimsiblingDependant { get; set; }
        public Nullable<System.DateTime> victimsiblingDateOfBirth { get; set; }
        public string victimsiblingGender { get; set; }
        public string victimsiblingOccupation { get; set; }

        //beneficiary deets
        public string beneficiaryUserFirst { get; set; }
        public string beneficiaryUserLast { get; set; }
        public string beneficiaryUserMiddle { get; set; }
        public int beneficiaryPhoneNumber { get; set; }
        public Nullable<System.DateTime> beneficiaryDateOfBirth { get; set; }
        public string beneficiaryPlaceOfBirth { get; set; }
        public string beneficiaryGender { get; set; }
        public string beneficiaryEmail { get; set; }
        public bool beneficiaryDependant { get; set; }
        public string beneficiaryOccupation { get; set; }
        public int beneficiaryAge { get; set; }

        //ROE deets
        public string ROEUserFirst { get; set; }
        public string ROEUserLast { get; set; }
        public string ROEUserMiddle { get; set; }
        public int ROEPhoneNumber { get; set; }
        public Nullable<System.DateTime> ROEDateOfBirth { get; set; }
        public string ROEPlaceOfBirth { get; set; }
        public string ROEGender { get; set; }
        public string ROEEmail { get; set; }
        public bool ROEDependant { get; set; }
        public string ROEOccupation { get; set; }
        public int ROEAge { get; set; }
        public int ROEUserTypeID { get; set; }

        //contestor deets
        public string contestorUserFirst { get; set; }
        public string contestorUserLast { get; set; }
        public string contestorUserMiddle { get; set; }
        public int contestorPhoneNumber { get; set; }
        public Nullable<System.DateTime> contestorDateOfBirth { get; set; }
        public string contestorPlaceOfBirth { get; set; }
        public string contestorGender { get; set; }
        public string contestorEmail { get; set; }
        public bool contestorDependant { get; set; }
        public string contestorOccupation { get; set; }
        public int contestorAge { get; set; }

        //individual deets
        public string Gender { get; set; }
        public string Occupation { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public bool Dependent { get; set; }
        public string vicEmployerName { get; set; }
        public int EmployerNumber { get; set; }
        public int PhoneNumber { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DateAdded { get; set; }
        public string GuardianshipDetails { get; set; }
        public int IndividualRelationID { get; set; }
        public int TortCaseID { get; set; }
        public int UserTypeID { get; set; }
        public int CompanionshipTypeID { get; set; }

        public IEnumerable<CompanionshipType> CompanionshipTypes { get; set; }
        public IEnumerable<UserType> UserTypes { get; set; }

        public int timesmarried { get; set; }

        // from address table

        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        //client residential address
        public string resclientStreetNumber { get; set; }
        public string resclientStreetName { get; set; }
        public string resclientSuburb { get; set; }
        public string resclientPostCode { get; set; }
        public string resclientState { get; set; }
        public string resclientCountry { get; set; }

        // client postal address
        public string postalclientStreetNumber { get; set; }
        public string postalclientStreetName { get; set; }
        public string postalclientSuburb { get; set; }
        public string postalclientPostCode { get; set; }
        public string postalclientState { get; set; }
        public string postalclientCountry { get; set; }

        //address at time of event for client
        public string eventclientStreetNumber { get; set; }
        public string eventclientStreetName { get; set; }
        public string eventclientSuburb { get; set; }
        public string eventclientPostCode { get; set; }
        public string eventclientState { get; set; }
        public string eventclientCountry { get; set; }

        // child address
        public string childStreetNumber { get; set; }
        public string childStreetName { get; set; }
        public string childSuburb { get; set; }
        public string childPostCode { get; set; }
        public string childState { get; set; }
        public string childCountry { get; set; }

        public int AddressTypeID { get; set; }

        //victim late spouse address
        public string VLSStreetNumber { get; set; }
        public string VLSStreetName { get; set; }
        public string VLSSuburb { get; set; }
        public string VLSPostCode { get; set; }
        public string VLSState { get; set; }
        public string VLSCountry { get; set; }

        //companion address
        public string compStreetNumber { get; set; }
        public string compStreetName { get; set; }
        public string compSuburb { get; set; }
        public string compPostCode { get; set; }
        public string compState { get; set; }
        public string compCountry { get; set; }

        //companion postal
        public string postalcompStreetNumber { get; set; }
        public string postalcompStreetName { get; set; }
        public string postalcompSuburb { get; set; }
        public string postalcompPostCode { get; set; }
        public string postalcompState { get; set; }
        public string postalcompCountry { get; set; }

        //victim mother address
        public string victimmotherStreetNumber { get; set; }
        public string victimmotherStreetName { get; set; }
        public string victimmotherSuburb { get; set; }
        public string victimmotherPostCode { get; set; }
        public string victimmotherState { get; set; }
        public string victimmotherCountry { get; set; }

        //victim father address
        public string victimfatherStreetNumber { get; set; }
        public string victimfatherStreetName { get; set; }
        public string victimfatherSuburb { get; set; }
        public string victimfatherPostCode { get; set; }
        public string victimfatherState { get; set; }
        public string victimfatherCountry { get; set; }

        //victimsibling address
        public string victimsiblingStreetNumber { get; set; }
        public string victimsiblingStreetName { get; set; }
        public string victimsiblingSuburb { get; set; }
        public string victimsiblingPostCode { get; set; }
        public string victimsiblingState { get; set; }
        public string victimsiblingCountry { get; set; }
        public int victimsiblingNumberofSiblings { get; set; }
        public int victimsiblingAge { get; set; }

        //beneficiary address
        public string beneficiaryStreetNumber { get; set; }
        public string beneficiaryStreetName { get; set; }
        public string beneficiarySuburb { get; set; }
        public string beneficiaryPostCode { get; set; }
        public string beneficiaryState { get; set; }
        public string beneficiaryCountry { get; set; }

        //ROE address
        public string ROEStreetNumber { get; set; }
        public string ROEStreetName { get; set; }
        public string ROESuburb { get; set; }
        public string ROEPostCode { get; set; }
        public string ROEState { get; set; }
        public string ROECountry { get; set; }

        public IEnumerable<AddressType> AddressTypes { get; set; }

        // from document
        public string DocumentFile { get; set; }
        public int DocumentTypeID { get; set; }
        public IEnumerable<DocumentType> DocumentTypes { get; set; }

        //victimeducation
        public string PrimarySchool { get; set; }
        public string SecondarySchool { get; set; }
        public string College { get; set; }
        public string TechnicalCollege { get; set; }
        public string University { get; set; }
        public string PostgraduateStudy { get; set; }
        public string OtherEducationalStudy { get; set; }
        public bool ReceivedAwards { get; set; }
        public int NumberOfAwards { get; set; }
        public string DetailsOfAwards { get; set; }

        // from defactocompanion
        public string DefactoCompanionID { get; set; }
        public DateTime DateOfCommencementOfRelationship { get; set; }
        public bool RelationshipContinued { get; set; }
        public int FacsimileNumber { get; set; }
        public int NumberOfChildren { get; set; }
        
        // add me to DB
        public DateTime DateOfMarriage { get; set; }

        // from incomes
        public string MainSourceOfIncome { get; set; }
        public int AmountOfGrossIncomeOver5Years { get; set; }
        public string DetailsOfIncome { get; set; }
        public Nullable<System.DateTime> CurrentFinancialYear { get; set; }

        // from medical history
        public string MedicalConditionAtTimeOfIncident { get; set; }
        public string CurrentMedicalConditions { get; set; }
        public string NameOfDoctor { get; set; }
        public string AddressOfDoctor { get; set; }
        public string TreatmentAfterEvent { get; set; }
        public bool HospitalizationAfterEvent { get; set; }
        public string PostMortemConducted { get; set; }
        public string PostMortemLocation { get; set; }
        public string PostMortemConductor { get; set; }
        public string WillAndTestament { get; set; }

        //add to medical history
        public bool ConditionAtTimeOfIncident { get; set; }
        public bool receivedTreatmentAfterEvent { get; set; }
        public bool currentlysufferingfromMedicalConditions { get; set; }
        public bool boolPostMortemConducted { get; set; }
        public string boolWillAndTestament { get; set; }

        // from payments
        public bool Received { get; set; }
        public int NumberOfPayment { get; set; }
        public int AmountOfPayment { get; set; }
        public bool DocumentsSigned { get; set; }

        // from postmortemservices
        public bool VictimDeceased { get; set; }
        public Nullable<System.DateTime> DateOfService { get; set; }
        public string LocationOfService { get; set; }
        public int CostOfService { get; set; }
        public bool Memorial { get; set; }
        public string LocationOfMemorial { get; set; }
        public int CostOfMemorial { get; set; }

        // from representativeofestates
        public bool LetterOfAdministration { get; set; }
        public string LocationOfLetterIssue { get; set; }
        public bool Contestors { get; set; }
        public string CourtName { get; set; }
        public int numberofContestors { get; set; }
        public string detailsofrelationship { get; set; }
        public bool grievancefiled { get; set; }
    }
}